package me.stepbystep.mgapi.lobby

import me.stepbystep.api.*
import me.stepbystep.api.chat.RED
import me.stepbystep.api.chat.broadcast
import me.stepbystep.api.chat.message1
import me.stepbystep.mgapi.common.packet.type.BroadcastMessagePacket
import me.stepbystep.mgapi.common.packet.type.CoreRestartPacket
import me.stepbystep.mgapi.common.util.handlePacket
import org.bukkit.Bukkit
import org.bukkit.scheduler.BukkitRunnable
import kotlin.time.Duration

fun LobbyActor.handlePackets() {
    BroadcastMessagePacket.handleFor(this)

    val restartMessage = message1<Int>(
        russian = { secondsLeft ->
            val secWord = secondsLeft.wordForNum("секунду", "секунды", "секунд")
            "${RED}Сервер будет перезагружен через $secondsLeft $secWord"
        },
        english = { secondsLeft ->
            val secWord = secondsLeft.englishWordForNum("second", "seconds")
            "${RED}Server will be restarted in $secondsLeft $secWord"
        },
    )

    packetHandler.handlePacket<CoreRestartPacket> { _, _ ->
        shutdown()

        object : BukkitRunnable() {
            private var secondsLeft = 10

            override fun run() {
                restartMessage.broadcast(secondsLeft)
                secondsLeft--
                if (secondsLeft == 0) {
                    performShutdown()
                    cancel()
                }
            }
        }.runTaskTimer(plugin, Duration.ZERO, 1.seconds)
    }
}

private fun LobbyActor.performShutdown() {
    Bukkit.getOnlinePlayers().forEach {
        it.performCommand("hub")
    }
    plugin.runDelayed(1.seconds, Bukkit::shutdown)
}
