package me.stepbystep.mgapi.lobby.command

import me.stepbystep.api.command.RequirementIsOperator
import me.stepbystep.api.command.executesWrapped
import me.stepbystep.api.command.register
import me.stepbystep.mgapi.common.packet.type.GetCoreRealmPacket
import me.stepbystep.mgapi.lobby.LobbyActor
import ru.cristalix.core.command.CommandHelper
import ru.cristalix.core.transfer.ITransferService

class TeleportToCoreCommand(actor: LobbyActor) {
    init {
        actor.messageTransport.sendPacket(GetCoreRealmPacket()).thenAccept { packet ->
            val realm = packet.serverData.realmId
            CommandHelper.literal("tpcore")
                .requires(RequirementIsOperator())
                .executesWrapped {
                    ITransferService.get().transfer(it.source, realm)
                }
                .register()
        }
    }
}
