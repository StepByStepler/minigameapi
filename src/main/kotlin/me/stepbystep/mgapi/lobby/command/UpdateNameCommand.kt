package me.stepbystep.mgapi.lobby.command

import com.mojang.brigadier.LiteralMessage
import com.mojang.brigadier.Requirement
import me.stepbystep.api.chat.*
import me.stepbystep.api.command.*
import me.stepbystep.mgapi.common.packet.type.SetPlayerNamePacket
import me.stepbystep.mgapi.lobby.LobbyActor
import org.bukkit.Bukkit
import ru.cristalix.core.command.CommandHelper
import ru.cristalix.core.permissions.IPermissionService
import ru.cristalix.core.permissions.StaffGroups
import java.util.*

class UpdateNameCommand(actor: LobbyActor) {
    companion object {
        const val HIDDEN_METADATA = "hiddenPlayer"
    }

    private val changedMessage = message0(
        russian = "Вы успешно обновили никнейм",
        english = "You successfully changed nickname",
    ).map { "${GREEN}$it" }

    init {
        val commandBuilder = CommandHelper.literal("setnick")
            .description("Сменить никнейм")
            .requires(CommandRequirement())

        fun updateName(uuid: UUID, newName: String?) {
            actor.messageTransport.sendPacket(SetPlayerNamePacket(SetPlayerNamePacket.ClientData(uuid, newName)))
            Bukkit.getPlayer(uuid).sendMessage(changedMessage)
        }

        commandBuilder.addChild("reset", "Восстановить никнейм") {
            updateName(it.source, null)
        }

        commandBuilder.thenWithParameters(stringParameter("Никнейм")) {
            it.executesWrapped { ctx ->
                val newName = ctx.getArgument<String>("Никнейм")
                if (newName.length > 16) {
                    ctx.sender.sendMessage("${RED}Ник не может быть длиннее 16 символов")
                    return@executesWrapped
                }
                updateName(ctx.source, newName)
            }
        }

        commandBuilder.register()
    }

    private class CommandRequirement : Requirement<UUID> {
        private val errorMessage = LiteralMessage("${RED}Вы не являетесь администратором или ютубером")

        override fun test(uuid: UUID): Boolean {
            val staffGroup = IPermissionService.get().getStaffGroup(uuid).get()
            if (staffGroup >= StaffGroups.ADMIN || staffGroup == StaffGroups.YOUTUBE) return true

            return Bukkit.getPlayer(uuid).hasMetadata(HIDDEN_METADATA)
        }

        override fun getErrorMessage(uuid: UUID?) = errorMessage
    }
}
