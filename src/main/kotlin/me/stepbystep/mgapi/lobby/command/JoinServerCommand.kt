package me.stepbystep.mgapi.lobby.command

import me.stepbystep.api.command.*
import me.stepbystep.mgapi.lobby.LobbyActor
import ru.cristalix.core.command.CommandHelper
import ru.cristalix.core.realm.RealmId

class JoinRealmCommand(private val actor: LobbyActor) {
    init {
        CommandHelper.literal("joinrealm")
            .requires(RequirementIsOperator())
            .apply {
                thenWithParameters(stringParameter("название реалма")) { builder ->
                    builder.executesWrapped { ctx ->
                        val realmName = ctx.getArgument<String>("название реалма")
                        val realm = RealmId.of(realmName)
                        actor.networkMessenger.sendPlayerToRealm(ctx.source, realm)
                    }
                }
            }
            .register()
    }
}
