package me.stepbystep.mgapi.lobby.model

import me.stepbystep.api.runRepeating
import me.stepbystep.api.seconds
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.model.OnlineDataEntry
import me.stepbystep.mgapi.common.packet.type.GetOnlineDataPacket
import me.stepbystep.mgapi.lobby.LobbyActor
import me.stepbystep.mgapi.lobby.event.OnlineDataUpdateEvent
import org.bukkit.Bukkit
import kotlin.time.Duration

class ServerOnlineDataHolder(private val actor: LobbyActor) {
    private val currentData = hashMapOf<GameType, OnlineDataEntry>()

    init {
        actor.plugin.runRepeating(Duration.ZERO, 15.seconds, ::syncData)
    }

    operator fun get(gameType: GameType): OnlineDataEntry =
        currentData[gameType] ?: error("Data for game type $gameType doesn't exist in $currentData!")

    private fun syncData() {
        actor.messageTransport.sendPacket(GetOnlineDataPacket()).thenAccept {
            currentData.putAll(it.serverData.servers)
            Bukkit.getPluginManager().callEvent(OnlineDataUpdateEvent(currentData.toMap()))
        }
    }
}