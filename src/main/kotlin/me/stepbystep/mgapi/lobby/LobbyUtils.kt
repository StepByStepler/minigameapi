package me.stepbystep.mgapi.lobby

import me.stepbystep.api.getOrCreateConfig
import me.stepbystep.mgapi.common.ServerID
import me.stepbystep.mgapi.common.ServerInfo
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.game.GameTypeHandler
import me.stepbystep.mgapi.common.packet.type.AddPlayerToQueuePacket
import me.stepbystep.mgapi.common.packet.type.RemovePlayerFromQueuePacket
import me.stepbystep.mgapi.core.CoreActor
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.plugin.Plugin
import java.util.*

const val MINIGAME_CONFIG_FILE = "minigame.yml"

inline fun <reified T : ServerInfo> CoreActor.getServerInfo(serverID: ServerID): T = serverID.getServerInfo()

val GameTypeHandler.maxPlayers: Int
    get() = allTypes.maxOf { it.maxPlayers }

val Plugin.minigameConfig: FileConfiguration
    get() = getOrCreateConfig(MINIGAME_CONFIG_FILE)

fun LobbyActor.addPlayerToQueue(uuid: UUID, gameType: GameType) {
    val packet = AddPlayerToQueuePacket(AddPlayerToQueuePacket.ClientData(uuid, gameType))
    messageTransport.sendPacket(packet)
}

fun LobbyActor.removePlayerFromQueue(uuid: UUID) {
    val packet = RemovePlayerFromQueuePacket(RemovePlayerFromQueuePacket.ClientData(uuid))
    messageTransport.sendPacket(packet)
}
