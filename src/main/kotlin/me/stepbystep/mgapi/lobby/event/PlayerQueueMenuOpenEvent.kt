package me.stepbystep.mgapi.lobby.event

import me.stepbystep.api.objects.Cancellable
import org.bukkit.entity.Player
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

class PlayerQueueMenuOpenEvent(val player: Player) : Event(), Cancellable by Cancellable.default() {
    companion object {
        private val handlerList = HandlerList()

        @JvmStatic
        fun getHandlerList() = handlerList
    }

    override fun getHandlers() = handlerList
}