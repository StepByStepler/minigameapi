package me.stepbystep.mgapi.lobby.event

import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.model.OnlineDataEntry
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

class OnlineDataUpdateEvent(val newData: Map<GameType, OnlineDataEntry>) : Event() {
    companion object {
        private val handlerList = HandlerList()

        @JvmStatic
        fun getHandlerList() = handlerList
    }

    override fun getHandlers() = handlerList
}