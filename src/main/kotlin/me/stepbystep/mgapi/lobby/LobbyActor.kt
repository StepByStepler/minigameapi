package me.stepbystep.mgapi.lobby

import com.google.common.collect.HashBiMap
import me.stepbystep.api.item.NMSItemStack
import me.stepbystep.api.item.asNewStack
import me.stepbystep.api.register
import me.stepbystep.api.registerServiceIfAbsent
import me.stepbystep.api.runDelayed
import me.stepbystep.api.seconds
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.game.GameTypeHandler
import me.stepbystep.mgapi.common.packet.type.HandshakePacket
import me.stepbystep.mgapi.common.util.sendPlayerToHub
import me.stepbystep.mgapi.lobby.command.JoinRealmCommand
import me.stepbystep.mgapi.lobby.command.TeleportToCoreCommand
import me.stepbystep.mgapi.lobby.command.UpdateNameCommand
import me.stepbystep.mgapi.lobby.listener.*
import me.stepbystep.mgapi.lobby.model.ServerOnlineDataHolder
import me.stepbystep.mgapi.minecraft.MinecraftActor
import net.citizensnpcs.api.CitizensAPI
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin
import ru.cristalix.core.realm.IRealmService
import ru.cristalix.core.scoreboard.IScoreboardService
import ru.cristalix.core.scoreboard.ScoreboardService

class LobbyActor(
    plugin: Plugin,
    gameTypeHandler: GameTypeHandler
) : MinecraftActor(plugin, gameTypeHandler) {

    private lateinit var npcQueueListener: NpcQueueListener
    private lateinit var onlineDataHolder: ServerOnlineDataHolder

    override fun registerDefaultListener() {
        super.registerDefaultListener()
        LobbyDefaultListener().register(plugin)
    }

    @PublishedApi
    override fun setup() {
        LobbyListener(this).register(plugin)
        JoinRealmCommand(this)
        TeleportToCoreCommand(this)
        setupBukkit()
        handlePackets()

        if (realmId.typeName != "TEST") {
            IRealmService.get().currentRealmInfo.isLobbyServer = true
        }
    }

    override fun shutdown() {
        Bukkit.getOnlinePlayers().forEach {
            removePlayerFromQueue(it.uniqueId)
        }
        messageTransport.shutdown()
    }

    override fun handleDefaultHandshake() {
        val data = HandshakePacket.Data.Lobby(realmId)
        val packet = HandshakePacket(HandshakePacket.ClientData(data))
        messageTransport.sendPacket(packet)
    }

    fun registerNpcListener() {
        npcQueueListener = NpcQueueListener(plugin.minigameConfig, this).also { it.register(plugin) }
    }

    fun enableJoinQueueItem(
        itemType: Material = Material.COMPASS,
        itemSlot: Int = 0,
        getDisplayName: (Player) -> String,
        createGameTypeItem: (GameType) -> NMSItemStack = { NMSItemStack.a },
    ) {
        JoinQueueListener(
            actor = this,
            itemType = itemType,
            itemSlot = itemSlot,
            getDisplayName = getDisplayName,
            createGameTypeItem = createGameTypeItem,
        )
    }

    fun enableQueueChoiceItem(
        itemType: Material = Material.COMPASS,
        itemSlot: Int = 0,
        gameTypes: List<GameType>,
        getDisplayName: (Player) -> String,
        createGameTypeItem: (GameType) -> NMSItemStack,
    ) {
        ensureOnlineSyncInitialized()
        QueueChoiceListener(
            actor = this,
            itemType = itemType,
            itemSlot = itemSlot,
            getDisplayName = getDisplayName,
            createGameTypeItem = createGameTypeItem,
            onlineDataHolder = onlineDataHolder,
            gameTypes = gameTypes,
        )
    }

    fun enableHubItem(
        itemType: Material = Material.WATCH,
        itemSlot: Int = 8,
        getDisplayName: (Player) -> String,
    ) {
        LobbyCustomItemListener(
            stack = itemType.asNewStack(),
            itemTag = "quitHubItem",
            itemSlot = itemSlot,
            getDisplayName = getDisplayName,
        ) {
            sendPlayerToHub(it.uniqueId)
        }.register(plugin)
    }

    fun enableGameHolograms() {
        if (!::npcQueueListener.isInitialized) error("You must enable NpcQueueListener to use holograms!")
        ensureOnlineSyncInitialized()

        plugin.runDelayed(1.seconds) {
            val locations = HashBiMap.create(npcQueueListener.npcTypes).inverse().toMap()
                .mapValues { CitizensAPI.getNPCRegistry().getById(it.value).storedLocation }

            GameBannersListener(locations).register(plugin)
        }
    }

    override fun setupCristalixNetwork(minigameName: String, gameServersRealm: String) {
        super.setupCristalixNetwork(minigameName, gameServersRealm)

        registerServiceIfAbsent { ScoreboardService() }
        IScoreboardService.get().serverStatusBoard.displayName = minigameName

        if (realmId.typeName != "TEST") {
            IRealmService.get().currentRealmInfo.servicedServers = arrayOf(gameServersRealm)
        }
    }

    override fun enableCustomNames() {
        super.enableCustomNames()
        UpdateNameCommand(this)
    }

    private fun ensureOnlineSyncInitialized() {
        if (!::onlineDataHolder.isInitialized) {
            onlineDataHolder = ServerOnlineDataHolder(this)
        }
    }

    companion object {
        inline operator fun invoke(
            plugin: Plugin,
            gameTypeHandler: GameTypeHandler,
            action: LobbyActor.() -> Unit = {},
        ): LobbyActor {
            val actor = LobbyActor(plugin, gameTypeHandler)
            action(actor)
            actor.setup()
            return actor
        }
    }
}
