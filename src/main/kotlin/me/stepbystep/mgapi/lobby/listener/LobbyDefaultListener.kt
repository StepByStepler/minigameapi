package me.stepbystep.mgapi.lobby.listener

import me.stepbystep.api.cancel
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageEvent

class LobbyDefaultListener : Listener {
    @EventHandler
    fun EntityDamageEvent.handle() {
        cancel()
    }
}