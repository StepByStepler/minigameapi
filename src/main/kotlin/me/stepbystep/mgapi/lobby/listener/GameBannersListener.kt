package me.stepbystep.mgapi.lobby.listener

import me.func.mod.world.Banners
import me.func.mod.world.Banners.location
import me.func.protocol.data.element.Banner
import me.stepbystep.api.chat.AQUA
import me.stepbystep.api.withWord
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.lobby.event.OnlineDataUpdateEvent
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener

class GameBannersListener(locations: Map<GameType, Location>) : Listener {

    private val banners: Map<GameType, Banner>

    init {
        banners = locations.mapValues { (gameType, location) ->
            location.createBanner(gameType)
        }
    }

    private fun Location.createBanner(gameType: GameType) = Banners.new {
        location(this@createBanner.clone().add(0.0, 5.5, 0.0))
        height = 43
        weight = 75
        content = getBannerText(gameType, 0, 0)
    }

    private fun getBannerText(gameType: GameType, totalPlayers: Int, waitingPlayers: Int): String {
        return listOf(
            "$AQUA${gameType.displayName.russian}",
            totalPlayers.withWord("игрок", "игрока", "игроков"),
            "Ожидают: $waitingPlayers",
        ).joinToString(separator = "\n")
    }

    @EventHandler
    fun OnlineDataUpdateEvent.handle() {
        for ((gameType, onlineData) in newData) {
            val banner = banners[gameType] ?: continue
            val (totalPlayers, waitingPlayers) = onlineData
            banner.content = getBannerText(gameType, totalPlayers, waitingPlayers)

            for (player in Bukkit.getOnlinePlayers()) {
                Banners.hide(player, banner)
                Banners.show(player, banner)
            }
        }
    }
}
