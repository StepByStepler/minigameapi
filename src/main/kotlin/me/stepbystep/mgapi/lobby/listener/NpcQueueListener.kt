package me.stepbystep.mgapi.lobby.listener

import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.lobby.LobbyActor
import me.stepbystep.mgapi.lobby.addPlayerToQueue
import me.stepbystep.mgapi.lobby.event.PlayerQueueMenuOpenEvent
import net.citizensnpcs.api.event.NPCRightClickEvent
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener

class NpcQueueListener(config: FileConfiguration, private val actor: LobbyActor) : Listener {
    val npcTypes: Map<Int, GameType>

    init {
        val npcTypes = hashMapOf<Int, GameType>()
        val npcSection = config.getConfigurationSection("npc") ?: error("\"npc\" section not specified in $config")

        for (key in npcSection.getKeys(false)) {
            val gameType = actor.gameTypeHandler.allTypes.firstOrNull { it.id.equals(key, ignoreCase = true) }
                ?: error("GameType with id $key does not exist")
            val npcID = npcSection.getInt(key, -1).takeIf { it >= 0 }
                ?: error("npc id for $key is negative or not specified")
            npcTypes[npcID] = gameType
        }

        this.npcTypes = npcTypes
    }

    @EventHandler
    fun NPCRightClickEvent.handle() {
        val isCancelled = !PlayerQueueMenuOpenEvent(clicker).callEvent()
        if (isCancelled) return

        val gameType = npcTypes[npc.id] ?: return
        actor.addPlayerToQueue(clicker.uniqueId, gameType)
    }
}
