package me.stepbystep.mgapi.lobby.listener

import me.stepbystep.api.cancel
import me.stepbystep.mgapi.lobby.LobbyActor
import me.stepbystep.mgapi.lobby.removePlayerFromQueue
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerDropItemEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent

class LobbyListener(private val actor: LobbyActor) : Listener {

    @EventHandler(priority = EventPriority.LOW)
    fun PlayerJoinEvent.handle() {
        player.inventory.clear()
        player.inventory.heldItemSlot = 0
    }

    @EventHandler
    fun PlayerQuitEvent.handle() {
        actor.removePlayerFromQueue(player.uniqueId)
    }

    @EventHandler
    fun PlayerDropItemEvent.handle() {
        cancel()
    }
}
