package me.stepbystep.mgapi.lobby.listener

import me.stepbystep.api.asNMS
import me.stepbystep.api.cancel
import me.stepbystep.api.displayName
import me.stepbystep.api.isLeftClick
import me.stepbystep.api.item.NMSItemStack
import me.stepbystep.api.item.accessTag
import me.stepbystep.api.item.getNullableBoolean
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerJoinEvent
import java.util.concurrent.CompletableFuture

class LobbyCustomItemListener(
    private val createStack: (Player) -> CompletableFuture<NMSItemStack>,
    private val itemTag: String,
    private val itemSlot: Int,
    private val getDisplayName: (Player) -> String,
    private val interactAction: (Player) -> Unit,
) : Listener {

    constructor(
        stack: NMSItemStack,
        itemTag: String,
        itemSlot: Int,
        getDisplayName: (Player) -> String,
        interactAction: (Player) -> Unit,
    ) : this({ CompletableFuture.completedFuture(stack.cloneItemStack()) }, itemTag, itemSlot, getDisplayName, interactAction)

    @EventHandler
    fun PlayerJoinEvent.handle() {
        createStack(player).thenAccept { stack ->
            stack.accessTag {
                setBoolean(itemTag, true)
            }
            stack.displayName = getDisplayName(player)
            player.inventory.asNMS().setItem(itemSlot, stack)
        }
    }

    @EventHandler
    fun InventoryClickEvent.handle() {
        val tag = currentItem?.asNMS()?.tag ?: return
        if (tag.getNullableBoolean(itemTag) == true) {
            cancel()
        }
    }

    @EventHandler
    fun PlayerInteractEvent.handle() {
        if (action.isLeftClick()) return
        val tag = item?.asNMS()?.tag ?: return

        if (tag.getNullableBoolean(itemTag) == true) {
            interactAction(player)
        }
    }
}
