package me.stepbystep.mgapi.lobby.listener

import me.func.mod.Anime
import me.func.mod.ui.menu.button
import me.func.mod.ui.menu.choicer
import me.stepbystep.api.asCraftMirror
import me.stepbystep.api.chat.invoke
import me.stepbystep.api.chat.message0
import me.stepbystep.api.chat.message1
import me.stepbystep.api.item.NMSItemStack
import me.stepbystep.api.item.asNewStack
import me.stepbystep.api.register
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.util.openDistinct
import me.stepbystep.mgapi.lobby.LobbyActor
import me.stepbystep.mgapi.lobby.addPlayerToQueue
import me.stepbystep.mgapi.lobby.event.PlayerQueueMenuOpenEvent
import me.stepbystep.mgapi.lobby.model.ServerOnlineDataHolder
import org.bukkit.Material
import org.bukkit.entity.Player

class QueueChoiceListener(
    private val actor: LobbyActor,
    private val createGameTypeItem: (GameType) -> NMSItemStack,
    private val onlineDataHolder: ServerOnlineDataHolder,
    private val gameTypes: List<GameType>,
    itemType: Material,
    itemSlot: Int,
    getDisplayName: (Player) -> String,
) {
    init {
        LobbyCustomItemListener(
            stack = itemType.asNewStack(),
            itemTag = "queueChoicerItem",
            itemSlot = itemSlot,
            getDisplayName = getDisplayName,
            interactAction = ::onClick,
        ).register(actor.plugin)
    }

    private fun onClick(player: Player) {
        val isCancelled = !PlayerQueueMenuOpenEvent(player).callEvent()
        if (isCancelled) return

        val allTypes = actor.gameTypeHandler.allTypes
        if (allTypes.size == 1) {
            actor.addPlayerToQueue(player.uniqueId, allTypes.single())
        } else {
            openChoicer(player)
        }
    }

    private fun openChoicer(player: Player) {
        choicer {
            title = "Custom Steve Chaos"
            description = "Выберите игровой режим"
            storage = createChoicerButtons(player)
        }.openDistinct(player)
    }

    private fun createChoicerButtons(player: Player) = gameTypes.map { gameType ->
        val onlineData = onlineDataHolder[gameType]
        button {
            title = gameType.displayName(player)
            item = createGameTypeItem(gameType).asCraftMirror()
            hint = Messages.select(player)
            description(
                Messages.waiting(player, onlineData.waitingPlayers),
                Messages.online(player, onlineData.totalPlayers),
                Messages.servers(player, onlineData.totalServers),
            )
            onClick { player, _, _ ->
                actor.addPlayerToQueue(player.uniqueId, gameType)
                Anime.close(player)
            }
        }
    }.toMutableList()

    private object Messages {
        val waiting = message1<Int>(
            russian = { "Ожидают: $it" },
            english = { "Waiting: $it" },
        )

        val online = message1<Int>(
            russian = { "Онлайн: $it" },
            english = { "Online: $it" },
        )

        val servers = message1<Int>(
            russian = { "Серверов: $it" },
            english = { "Servers: $it" },
        )

        val select = message0(
            russian = "Выбрать",
            english = "Select",
        )
    }
}