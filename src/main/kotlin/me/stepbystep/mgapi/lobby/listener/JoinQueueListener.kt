package me.stepbystep.mgapi.lobby.listener

import me.stepbystep.api.item.NMSItemStack
import me.stepbystep.api.item.asNewStack
import me.stepbystep.api.menu.buildSharedMenu
import me.stepbystep.api.menu.menuItem
import me.stepbystep.api.register
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.lobby.LobbyActor
import me.stepbystep.mgapi.lobby.addPlayerToQueue
import me.stepbystep.mgapi.lobby.event.PlayerQueueMenuOpenEvent
import org.bukkit.Material
import org.bukkit.entity.Player

class JoinQueueListener(
    private val actor: LobbyActor,
    itemType: Material,
    itemSlot: Int,
    getDisplayName: (Player) -> String,
    createGameTypeItem: (GameType) -> NMSItemStack,
) {
    private val menu by lazy {
        buildSharedMenu {
            size = 27
            title = "Выбор игрового режима"

            val allTypes = actor.gameTypeHandler.allTypes
            val slots = when (allTypes.size) {
                2 -> intArrayOf(11, 15)
                3 -> intArrayOf(11, 13, 15)
                else -> error("Wrong amount of gameTypes (${allTypes.size})")
            }

            for ((index, gameType) in allTypes.withIndex()) {
                slots[index] bindTo menuItem(createGameTypeItem(gameType)) {
                    actor.addPlayerToQueue(whoClicked.uniqueId, gameType)
                    whoClicked.closeInventory()
                }
            }
        }
    }

    init {
        LobbyCustomItemListener(
            stack = itemType.asNewStack(),
            itemTag = "joinQueueItem",
            itemSlot = itemSlot,
            getDisplayName = getDisplayName,
            interactAction = ::onClick,
        ).register(actor.plugin)
    }

    private fun onClick(player: Player) {
        val isCancelled = !PlayerQueueMenuOpenEvent(player).callEvent()
        if (isCancelled) return

        val allTypes = actor.gameTypeHandler.allTypes
        if (allTypes.size == 1) {
            actor.addPlayerToQueue(player.uniqueId, allTypes.single())
        } else {
            menu.openFor(player)
        }
    }
}
