package me.stepbystep.mgapi.core.queue

import com.google.common.collect.HashMultimap
import com.google.common.collect.MultimapBuilder
import com.google.common.collect.Multimaps
import me.stepbystep.api.*
import me.stepbystep.api.chat.*
import me.stepbystep.mgapi.common.ServerID
import me.stepbystep.mgapi.common.ServerInfo
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.packet.type.AcceptPlayersStatusPacket
import me.stepbystep.mgapi.common.packet.type.AddPlayerToQueuePacket
import me.stepbystep.mgapi.common.packet.type.HandshakePacket
import me.stepbystep.mgapi.common.packet.type.RemovePlayerFromQueuePacket
import me.stepbystep.mgapi.common.util.handlePacket
import me.stepbystep.mgapi.common.util.sendMessage
import me.stepbystep.mgapi.core.CoreActor
import me.stepbystep.mgapi.core.event.PlayerJoinGameEvent
import me.stepbystep.mgapi.lobby.getServerInfo
import ru.cristalix.core.CoreApi
import ru.cristalix.core.party.IPartyService
import ru.cristalix.core.party.PartyService
import ru.cristalix.core.party.PartySnapshot
import java.util.*
import kotlin.time.Duration

class QueueHandler(private val actor: CoreActor, joinDelay: Duration) {
    private val servers = HashMultimap.create<GameType, ServerInfo.Game>()
    private val joinDelayMillis = joinDelay.inWholeMilliseconds

    @Suppress("UnstableApiUsage")
    private val queue = Multimaps.synchronizedListMultimap(
        MultimapBuilder.hashKeys().arrayListValues().build<GameType, QueuePlayerData>()
    )
    var banController: BanController? = null

    init {
        registerServiceIfAbsent { PartyService(CoreApi.get().socketClient) }

        actor.packetHandler.handlePacket<HandshakePacket> { _, clientID ->
            actor.plugin.runDelayed(2.seconds) {
                val serverInfo = actor.getServerInfo<ServerInfo>(clientID)
                if (serverInfo !is ServerInfo.Game || !serverInfo.canAcceptPlayers) return@runDelayed

                servers.put(serverInfo.gameType, serverInfo)
                updateQueue(serverInfo)
            }
        }

        actor.packetHandler.handlePacket<AcceptPlayersStatusPacket> { packet, clientID ->
            if (packet.clientData.state) {
                actor.plugin.runDelayed(1.seconds) {
                    updateQueue(actor.getServerInfo(clientID))
                }
            }
        }

        actor.packetHandler.handlePacket<AddPlayerToQueuePacket> { packet, _ ->
            val data = packet.clientData
            addPlayer(data.uuid, data.gameType)
        }

        actor.packetHandler.handlePacket<RemovePlayerFromQueuePacket> { packet, _ ->
            removePlayerFromQueue(packet.clientData.uuid)
        }
    }

    private fun updateQueue(serverInfo: ServerInfo.Game) {
        queue[serverInfo.gameType].toList().forEach {
            checkPlayer(it, serverInfo.gameType)
        }
    }

    private fun getBestServer(gameType: GameType, partySize: Int): ServerInfo? =
        servers[gameType]
            .asSequence()
            .filter { it.canAcceptPlayers && !it.isHidden }
            .filter { it.joinedPlayers + partySize <= it.gameType.maxPlayers }
            .maxByOrNull(ServerInfo::joinedPlayers)

    private fun addPlayer(playerUUID: UUID, gameType: GameType) {
        if (playerUUID.isBanned()) return

        if (queue[gameType].any { it.uuid == playerUUID }) {
            actor.networkMessenger.sendMessage(playerUUID, Messages.alreadyInQueue)
            return
        }

        val queueData = QueuePlayerData(playerUUID, millisNow() + joinDelayMillis)
        queue[gameType] += queueData
        if (joinDelayMillis != 0.toLong()) {
            actor.networkMessenger.sendMessage(playerUUID, Messages.searchingForGame)
        }
        actor.plugin.runDelayed(joinDelayMillis.milliseconds + 10.ticks) { // make sure join delay is passed
            checkPlayer(queueData, gameType)
        }
    }

    private fun checkPlayer(queueData: QueuePlayerData, gameType: GameType) {
        val (playerUUID, allowJoinTimestamp) = queueData
        if (allowJoinTimestamp > millisNow()) return
        if (queueData !in queue.values()) return

        IPartyService.get().getPartyByMember(playerUUID).thenAccept { optionalParty ->
            val partyMembers = optionalParty
                .filter { it.leader == playerUUID }
                .map { it.sortedMembers() }
                .orElseGet { listOf(playerUUID) }

            val bestServer = getBestServer(gameType, partyMembers.size)

            if (bestServer != null) {
                partyMembers.forEach {
                    if (it.isBanned()) return@forEach

                    val message = when (banController) {
                        null -> Messages.gameFoundWithoutPunish
                        else -> Messages.gameFoundWithPunish
                    }

                    PlayerJoinGameEvent(it).callEvent()
                    actor.networkMessenger.sendPlayerToRealm(it, bestServer.realm)
                    actor.networkMessenger.sendMessage(it, message)
                }
            }
        }
    }

    private fun removePlayerFromQueue(playerUUID: UUID) {
        queue.values().removeIf { it.uuid == playerUUID }
    }

    private fun PartySnapshot.sortedMembers(): List<UUID> {
        val membersWithoutLeader = members - leader
        return listOf(leader, *membersWithoutLeader.toTypedArray())
    }

    private fun UUID.isBanned(): Boolean = banController?.isBanned(this) == true

    fun removeServer(serverID: ServerID) {
        servers.values().removeIf { it.id == serverID }
    }

    private data class QueuePlayerData(
        val uuid: UUID,
        val allowJoinTimestamp: Long,
    )

    private object Messages {
        val alreadyInQueue = message0(
            russian = "Вы уже встали в очередь",
            english = "You are already in queue",
        ).prefixed(RED)

        val searchingForGame = message0(
            russian = "Ищем вам игру...",
            english = "Searching for game...",
        ).prefixed(WHITE)

        val gameFoundWithPunish = message0(
            russian = "Игра найдена, за выход из активной игры Вы получите штраф",
            english = "Game is found, you will be punished for exiting active game",
        ).prefixed(GREEN)

        val gameFoundWithoutPunish = message0(
            russian = "Игра найдена",
            english = "Game is found",
        ).prefixed(GREEN)
    }
}
