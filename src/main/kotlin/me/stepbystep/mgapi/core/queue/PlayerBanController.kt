package me.stepbystep.mgapi.core.queue

import com.google.common.cache.CacheBuilder
import me.stepbystep.api.chat.GOLD
import me.stepbystep.api.chat.RED
import me.stepbystep.api.chat.message1
import me.stepbystep.api.englishWordForNum
import me.stepbystep.api.millisNow
import me.stepbystep.api.toMinutesAndSecondsString
import me.stepbystep.mgapi.common.packet.type.BanPlayerPacket
import me.stepbystep.mgapi.common.util.handlePacket
import me.stepbystep.mgapi.core.CoreActor
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.time.Duration

class PlayerBanController(
    private val actor: CoreActor,
    private val delay: Duration,
) : BanController(actor) {
    private val playerBans = CacheBuilder.newBuilder()
        .expireAfterWrite(delay.inWholeMilliseconds, TimeUnit.MILLISECONDS)
        .build<UUID, Long>()

    private val bannedMessage = message1<Long>(
        russian = {
            val leftTime = it.toMinutesAndSecondsString()
            "${RED}Вы не можете начать новую игру, потому что недавно вышли во время игры. Осталось: $GOLD$leftTime"
        },
        english = { millisLeft ->
            val leftTime = millisLeft.toMinutesAndSecondsString(
                getMinutesWord = { it.englishWordForNum("minute", "minutes") },
                getSecondsWord = { it.englishWordForNum("second", "seconds") }
            )
            "${RED}You cannot start a new game because you recently exited while playing. Left time: $GOLD$leftTime"
        },
    )

    init {
        actor.packetHandler.handlePacket<BanPlayerPacket> { packet, _ ->
            ban(packet.clientData.playerUUID)
        }
    }

    override fun ban(playerUUID: UUID) {
        playerBans.put(playerUUID, millisNow())
    }

    override fun isBanned(playerUUID: UUID): Boolean {
        val lastBan = playerBans.getIfPresent(playerUUID) ?: return false
        val leftMillis = (lastBan + delay.inWholeMilliseconds) - millisNow()
        if (leftMillis <= 0) return false

        val text = bannedMessage(playerUUID, leftMillis)
        actor.networkMessenger.sendMessage(playerUUID, text)

        return true
    }

    override fun cancelLastBan(playerUUID: UUID) {
        playerBans.invalidate(playerUUID)
    }
}
