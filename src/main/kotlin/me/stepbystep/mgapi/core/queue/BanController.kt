package me.stepbystep.mgapi.core.queue

import me.stepbystep.mgapi.common.packet.type.BanPlayerPacket
import me.stepbystep.mgapi.common.util.handlePacket
import me.stepbystep.mgapi.core.CoreActor
import java.util.*

abstract class BanController(actor: CoreActor) {

    init {
        actor.packetHandler.handlePacket<BanPlayerPacket> { packet, _ ->
            ban(packet.clientData.playerUUID)
        }
    }

    abstract fun ban(playerUUID: UUID)
    abstract fun isBanned(playerUUID: UUID): Boolean
    abstract fun cancelLastBan(playerUUID: UUID)
}