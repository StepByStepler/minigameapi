package me.stepbystep.mgapi.core

import me.stepbystep.api.days
import me.stepbystep.api.milliseconds
import me.stepbystep.api.runRepeating
import me.stepbystep.api.timeNow
import me.stepbystep.mgapi.common.Actor
import me.stepbystep.mgapi.common.ServerID
import me.stepbystep.mgapi.common.ServerInfo
import me.stepbystep.mgapi.common.ServerSide
import me.stepbystep.mgapi.common.game.GameTypeHandler
import me.stepbystep.mgapi.common.packet.type.CoreRestartPacket
import me.stepbystep.mgapi.common.packet.type.HandshakePacket
import me.stepbystep.mgapi.common.packet.type.ScheduleRestartPacket
import me.stepbystep.mgapi.common.util.handlePacketWithResponse
import me.stepbystep.mgapi.core.name.CustomNameHandler
import me.stepbystep.mgapi.core.queue.BanController
import me.stepbystep.mgapi.core.queue.PlayerBanController
import me.stepbystep.mgapi.core.queue.QueueHandler
import me.stepbystep.mgapi.core.transport.CoreMessageTransport
import me.stepbystep.mgapi.core.transport.netty.CoreNettyTransport
import me.stepbystep.mgapi.lobby.minigameConfig
import org.bukkit.plugin.Plugin
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.concurrent.CompletableFuture
import kotlin.time.Duration

@Suppress("unused")
class CoreActor @PublishedApi internal constructor(
    plugin: Plugin,
    gameTypeHandler: GameTypeHandler,
    queueJoinDelay: Duration,
) : Actor<CoreMessageTransport>(plugin, gameTypeHandler) {

    val port = plugin.minigameConfig.getInt("lobby.port")
    override var messageTransport: CoreMessageTransport = CoreNettyTransport(this)
    val queueHandler = QueueHandler(this, queueJoinDelay)
    private var customNameHandler: CustomNameHandler? = null

    @PublishedApi
    override fun setup() {
        registerPacketListeners()
        packetHandler.handshakeAction = messageTransport::registerServer
        CoreRestartPacket() // load class, so it can be used later
        setupBukkit()
    }

    override fun shutdown() {
        messageTransport.broadcastPacket(CoreRestartPacket())
        messageTransport.shutdown()
    }

    override fun handleDefaultHandshake() {
        packetHandler.handlePacketWithResponse<HandshakePacket> { _, _ ->
            CompletableFuture.completedFuture(HandshakePacket(HandshakePacket.ServerData()))
        }
    }

    fun enableBanController(delay: Duration) {
        queueHandler.banController = PlayerBanController(this, delay)
    }

    fun enableCustomBanController(controller: BanController) {
        queueHandler.banController = controller
    }

    fun enableCustomNames() {
        customNameHandler = CustomNameHandler(this)
    }

    fun scheduleMidnightRestart() {
        val nextRestart = Instant.now()
            .plus(1, ChronoUnit.DAYS)
            .truncatedTo(ChronoUnit.DAYS)
            .toEpochMilli()

        val firstDifference = nextRestart.milliseconds - timeNow()

        plugin.runRepeating(firstDifference, 1.days) {
            messageTransport.broadcastPacket(ScheduleRestartPacket(), ServerSide.Game)
        }
    }

    inline fun <reified T : ServerInfo> ServerID.getServerInfo(): T {
        val serverInfo = messageTransport.getServerInfo(this)
        check(serverInfo is T) { "$serverInfo is not ${T::class.java} (clientID = $this)" }
        return serverInfo
    }

    inline fun <reified T : ServerInfo> ServerID.getServerInfoSafe(): T? {
        val serverInfo = messageTransport.getServerInfoSafe(this)
        return if (serverInfo is T) serverInfo else null
    }

    companion object {
        inline operator fun invoke(
            plugin: Plugin,
            gameTypeHandler: GameTypeHandler,
            queueJoinDelay: Duration = Duration.ZERO,
            action: CoreActor.() -> Unit = {},
        ): CoreActor {
            val actor = CoreActor(plugin, gameTypeHandler, queueJoinDelay)
            action(actor)
            actor.setup()
            return actor
        }
    }
}
