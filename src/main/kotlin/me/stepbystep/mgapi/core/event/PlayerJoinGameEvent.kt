package me.stepbystep.mgapi.core.event

import org.bukkit.event.Event
import org.bukkit.event.HandlerList
import java.util.*

class PlayerJoinGameEvent(val playerUUID: UUID) : Event() {
    companion object {
        private val handlerList = HandlerList()

        @JvmStatic
        fun getHandlerList() = handlerList
    }

    override fun getHandlers() = handlerList
}

