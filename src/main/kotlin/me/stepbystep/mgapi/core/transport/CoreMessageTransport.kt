package me.stepbystep.mgapi.core.transport

import me.stepbystep.mgapi.common.MessageTransport
import me.stepbystep.mgapi.common.ServerID
import me.stepbystep.mgapi.common.ServerInfo
import me.stepbystep.mgapi.common.ServerSide
import me.stepbystep.mgapi.common.packet.Packet
import me.stepbystep.mgapi.common.packet.type.HandshakePacket

interface CoreMessageTransport : MessageTransport {
    fun registerServer(serverID: ServerID, packet: HandshakePacket)
    fun getServerInfoSafe(serverID: ServerID): ServerInfo?
    fun broadcastPacket(packet: Packet<*, *>, predicate: (ServerInfo) -> Boolean = { true })
    fun sendPacket(serverID: ServerID, packet: Packet<*, *>)
    fun removeServer(id: ServerID)
    fun shutdown()
    fun getServers(): List<ServerInfo>

    fun broadcastPacket(packet: Packet<*, *>, side: ServerSide) {
        broadcastPacket(packet) {
            it.side == side
        }
    }

    fun getServerInfo(serverID: ServerID): ServerInfo {
        return getServerInfoSafe(serverID) ?: error("No server info found for $serverID")
    }
}
