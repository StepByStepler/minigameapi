package me.stepbystep.mgapi.core.transport.netty

import com.google.common.collect.HashBiMap
import com.google.common.collect.Maps
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.*
import io.netty.channel.epoll.Epoll
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollServerSocketChannel
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import me.stepbystep.mgapi.common.ServerID
import me.stepbystep.mgapi.common.ServerInfo
import me.stepbystep.mgapi.common.packet.Packet
import me.stepbystep.mgapi.common.packet.type.HandshakePacket
import me.stepbystep.mgapi.common.transport.netty.NettyChannelInitializer
import me.stepbystep.mgapi.common.util.writeAndFlushInEventLoop
import me.stepbystep.mgapi.core.CoreActor
import me.stepbystep.mgapi.core.transport.CoreMessageTransport
import java.util.concurrent.ConcurrentHashMap

class CoreNettyTransport(private val actor: CoreActor) : CoreMessageTransport {

    private val servers = ConcurrentHashMap<Channel, ServerInfo>()

    private val clients = Maps.synchronizedBiMap(HashBiMap.create<Channel, ServerID>())

    private val serverChannel: Channel = ServerBootstrap()
        .group(createEventLoopGroup(), createEventLoopGroup())
        .channel(if (Epoll.isAvailable()) EpollServerSocketChannel::class.java else NioServerSocketChannel::class.java)
        .childHandler(object : NettyChannelInitializer(actor) {
            override fun initChannel(ch: Channel) {
                super.initChannel(ch)
                clients[ch] = ServerID(ch.id().asLongText())
                ch.closeFuture().addListener {
                    removeServer(ch)
                }
            }

            override fun channelUnregistered(ctx: ChannelHandlerContext) {
                super.channelUnregistered(ctx)
                removeServer(ctx.channel())
            }
        })
        .option(ChannelOption.TCP_NODELAY, true)
        .option(ChannelOption.SO_KEEPALIVE, true)
        .option(ChannelOption.RCVBUF_ALLOCATOR, AdaptiveRecvByteBufAllocator())
        .bind(actor.port.also { println("binding to port: $it") })
        .sync()
        .channel()

    override fun registerServer(serverID: ServerID, packet: HandshakePacket) {
        val channel = serverID.getChannel()
        val serverInfo = packet.createServerInfo(serverID)
        removeConflictingServers(serverInfo)
        servers[channel] = serverInfo
    }

    override fun getServerInfoSafe(serverID: ServerID) = servers[serverID.getChannel()]

    override fun broadcastPacket(packet: Packet<*, *>, predicate: (ServerInfo) -> Boolean) {
        servers.forEach { (channel, serverInfo) ->
            if (predicate(serverInfo)) {
                channel.writeAndFlushInEventLoop(packet)
            }
        }
    }

    override fun sendPacket(serverID: ServerID, packet: Packet<*, *>) {
        serverID.getChannel().writeAndFlushInEventLoop(packet)
    }

    override fun removeServer(id: ServerID) {
        val ch = clients.inverse()[id] ?: error("Server for $id does not exists")
        actor.queueHandler.removeServer(getClientID(ch))
        servers -= ch
        clients -= ch
    }

    override fun shutdown() {
        serverChannel.close()
    }

    override fun getClientID(client: Any): ServerID {
        require(client is Channel) {
            "$client is not ${Channel::class.java.name}"
        }
        return clients[client] ?: error("ClientID not created for $client")
    }

    override fun getServers(): List<ServerInfo> = servers.values.toList()

    private fun createEventLoopGroup(): EventLoopGroup =
        if (Epoll.isAvailable()) EpollEventLoopGroup() else NioEventLoopGroup()

    private fun ServerID.getChannel(): Channel =
        clients.inverse()[this] ?: error("No Channel found for $this")

    private fun removeConflictingServers(serverInfo: ServerInfo) {
        servers.filterValues {
            it !== serverInfo && it.realm == serverInfo.realm
        }.keys.forEach {
            removeServer(getClientID(it))
        }
    }

    private fun removeServer(channel: Channel) {
        clients[channel]?.let(::removeServer)
    }
}
