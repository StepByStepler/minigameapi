package me.stepbystep.mgapi.core

import me.stepbystep.mgapi.client.GameState
import me.stepbystep.mgapi.common.ServerInfo
import me.stepbystep.mgapi.common.model.OnlineDataEntry
import me.stepbystep.mgapi.common.packet.type.*
import me.stepbystep.mgapi.common.util.handlePacket
import me.stepbystep.mgapi.common.util.handlePacketWithResponse
import me.stepbystep.mgapi.common.util.sendPlayerToHub
import ru.cristalix.core.realm.IRealmService
import ru.cristalix.core.realm.RealmId
import ru.cristalix.core.realm.RealmService
import ru.cristalix.core.realm.RealmStatus
import java.util.concurrent.CompletableFuture

internal fun CoreActor.registerPacketListeners() {
    packetHandler.handlePacket<BroadcastMessageToServersPacket> { packet, clientID ->
        val broadcastPacket = BroadcastMessagePacket(BroadcastMessagePacket.ServerData(packet.clientData.message))

        for (side in packet.clientData.sides) {
            messageTransport.broadcastPacket(broadcastPacket) {
                it.side == side && !(packet.clientData.skipSource && it.id == clientID)
            }
        }
    }

    packetHandler.handlePacket<UpdateOnlinePacket> { packet, clientID ->
        clientID.getServerInfo<ServerInfo.Game>().joinedPlayers = packet.clientData.onlinePlayers
    }

    packetHandler.handlePacket<AcceptPlayersStatusPacket> { packet, clientID ->
        clientID.getServerInfo<ServerInfo.Game>().canAcceptPlayers = packet.clientData.state
    }

    packetHandler.handlePacket<SendPlayerToLobbyPacket> { packet, _ ->
        val playerUUID = packet.clientData.playerUUID
        val bestLobby = getBestLobby()

        if (bestLobby != null) {
            networkMessenger.sendPlayerToRealm(playerUUID, bestLobby)
        } else {
            printRealmMap()
            sendPlayerToHub(playerUUID)
        }
    }

    packetHandler.handlePacketWithResponse<GetCoreRealmPacket> { _, _ ->
        val realmId = IRealmService.get().currentRealmInfo.realmId
        val packet = GetCoreRealmPacket(GetCoreRealmPacket.ServerData(realmId))
        CompletableFuture.completedFuture(packet)
    }

    packetHandler.handlePacketWithResponse<GetPlayersCountPacket> { packet, _ ->
        val gameType = packet.clientData.gameType
        val realmsWithType = messageTransport.getServers()
            .filter { it is ServerInfo.Game && it.gameType == gameType }
            .map { IRealmService.get().getRealmById(it.realm) }
        val totalPlayers = realmsWithType.sumOf { it.currentPlayers }
        val waitingPlayers = realmsWithType
            .filter { it.status == RealmStatus.WAITING_FOR_PLAYERS }
            .sumOf { it.currentPlayers }

        val serverData = GetPlayersCountPacket.ServerData(totalPlayers, waitingPlayers)
        CompletableFuture.completedFuture(GetPlayersCountPacket(serverData))
    }

    packetHandler.handlePacket<UpdateGameStatePacket> { packet, clientID ->
        clientID.getServerInfo<ServerInfo.Game>().gameState = packet.clientData.gameState
    }

    packetHandler.handlePacketWithResponse<GetOnlineDataPacket> { _, _ ->
        val data = gameTypeHandler.allTypes.associateWith { gameType ->
            val serversWithType = messageTransport.getServers()
                .filterIsInstance<ServerInfo.Game>()
                .filter { it.gameType == gameType }
            val waitingServers = serversWithType.filter { it.gameState == GameState.Waiting }

            OnlineDataEntry(
                totalPlayers = serversWithType.sumOf { it.joinedPlayers },
                waitingPlayers = waitingServers.sumOf { it.joinedPlayers },
                totalServers = serversWithType.size,
            )
        }

        CompletableFuture.completedFuture(GetOnlineDataPacket(GetOnlineDataPacket.ServerData(data)))
    }
}

private fun CoreActor.getBestLobby(): RealmId? {
    val lobbyRealmType = messageTransport.getServers()
        .filterIsInstance<ServerInfo.Lobby>()
        .mapTo(hashSetOf()) { it.realm.typeName }
        .singleOrNull()

    if (lobbyRealmType == null || lobbyRealmType == "TEST") return null

    val allRealms = IRealmService.get().getRealmsOfType(lobbyRealmType)
    return allRealms.minByOrNull { it.currentPlayers }?.realmId
}

private fun printRealmMap() {
    val service = IRealmService.get()
    val map = RealmService::class.java.getDeclaredField("realmMap").let {
        it.isAccessible = true
        it[service]
    }
    println(map)
}
