package me.stepbystep.mgapi.core.name

import me.stepbystep.mgapi.common.packet.type.SetPlayerNamePacket
import me.stepbystep.mgapi.common.packet.type.SyncPlayerNamesPacket
import me.stepbystep.mgapi.common.util.handlePacket
import me.stepbystep.mgapi.common.util.handlePacketWithResponse
import me.stepbystep.mgapi.core.CoreActor
import java.util.*
import java.util.concurrent.CompletableFuture

class CustomNameHandler(actor: CoreActor) {
    private val playerNames = hashMapOf<UUID, String>()

    init {
        actor.packetHandler.handlePacket<SetPlayerNamePacket> { packet, _ ->
            val (uuid, newName) = packet.clientData
            if (newName == null) {
                playerNames -= uuid
            } else {
                playerNames[uuid] = newName
            }

            actor.messageTransport.broadcastPacket(SyncPlayerNamesPacket(SyncPlayerNamesPacket.ServerData(playerNames)))
        }

        actor.packetHandler.handlePacketWithResponse<SyncPlayerNamesPacket> { _, _ ->
            CompletableFuture.completedFuture(SyncPlayerNamesPacket(SyncPlayerNamesPacket.ServerData(playerNames)))
        }
    }
}
