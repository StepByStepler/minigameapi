package me.stepbystep.mgapi.minecraft

import me.stepbystep.api.register
import me.stepbystep.mgapi.common.Actor
import me.stepbystep.mgapi.common.game.GameTypeHandler
import me.stepbystep.mgapi.common.packet.type.SyncPlayerNamesPacket
import me.stepbystep.mgapi.lobby.minigameConfig
import me.stepbystep.mgapi.minecraft.transport.MinecraftMessageTransport
import me.stepbystep.mgapi.minecraft.transport.netty.MinecraftNettyTransport
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.plugin.Plugin
import ru.cristalix.core.realm.IRealmService
import ru.cristalix.core.realm.RealmId
import java.net.InetSocketAddress

abstract class MinecraftActor(
    plugin: Plugin,
    gameTypeHandler: GameTypeHandler,
) : Actor<MinecraftMessageTransport>(plugin, gameTypeHandler) {

    val realmId: RealmId = IRealmService.get().currentRealmInfo.realmId
    val coreAddress: InetSocketAddress = InetSocketAddress.createUnresolved(
        plugin.minigameConfig.getString("lobby.host"),
        plugin.minigameConfig.getInt("lobby.port")
    )

    @Suppress("LeakingThis")
    final override var messageTransport: MinecraftMessageTransport = MinecraftNettyTransport(this)
    var spawnLocation: Location = Bukkit.getWorld("world").spawnLocation.toCenterLocation()

    private var defaultListener: DefaultListener? = null
    private var namesListener: SyncPlayerNamesPacket.JoinListener? = null

    open fun registerDefaultListener() {
        defaultListener = DefaultListener()
    }

    override fun setupBukkit() {
        super.setupBukkit()
        MinecraftListener(this).register(plugin)
        defaultListener?.register(plugin)
        namesListener?.register()
    }

    open fun enableCustomNames() {
        namesListener = SyncPlayerNamesPacket.JoinListener(this)
    }

    open fun setupCristalixNetwork(minigameName: String, gameServersRealm: String) {
        IRealmService.get().currentRealmInfo.groupName = minigameName
    }
}
