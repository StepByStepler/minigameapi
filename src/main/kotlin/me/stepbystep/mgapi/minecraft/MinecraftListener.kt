package me.stepbystep.mgapi.minecraft

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class MinecraftListener(private val actor: MinecraftActor) : Listener {
    @EventHandler
    fun PlayerJoinEvent.handle() {
        player.teleport(actor.spawnLocation)
    }
}
