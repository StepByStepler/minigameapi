package me.stepbystep.mgapi.minecraft

import me.stepbystep.api.cancel
import org.bukkit.Bukkit
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.World
import org.bukkit.entity.EntityType
import org.bukkit.entity.Projectile
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.*
import org.bukkit.event.entity.*
import org.bukkit.event.player.PlayerArmorStandManipulateEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.weather.WeatherChangeEvent
import org.bukkit.event.world.WorldLoadEvent
import org.spigotmc.SpigotConfig
import java.util.*

class DefaultListener : Listener {
    private val wrongBlockTypes = EnumSet.of(
        Material.NETHER_WARTS, Material.RED_ROSE, Material.YELLOW_FLOWER,
        Material.STRING, Material.DOUBLE_PLANT, Material.CARPET, Material.LONG_GRASS,
    )

    init {
        Bukkit.getWorlds().forEach { it.applySettings() }
        SpigotConfig.disabledAdvancements = listOf("*")
        SpigotConfig.disableAdvancementSaving = true
    }

    @EventHandler
    fun WeatherChangeEvent.handle() {
        isCancelled = toWeatherState()
    }

    @EventHandler
    fun FoodLevelChangeEvent.handle() {
        cancel()
    }

    @EventHandler
    fun LeavesDecayEvent.handle() {
        cancel()
    }

    @EventHandler
    fun BlockFromToEvent.handle() {
        if (toBlock.type in wrongBlockTypes) {
            cancel()
        }
    }

    @EventHandler
    fun BlockPhysicsEvent.handle() {
        if (changedType in wrongBlockTypes) {
            cancel()
        }
    }

    @EventHandler
    fun PlayerArmorStandManipulateEvent.handle() {
        cancel()
    }

    @EventHandler
    fun EntityCombustEvent.handle() {
        if (entity !is Projectile && this !is EntityCombustByEntityEvent && this !is EntityCombustByBlockEvent) {
            cancel()
        }
    }

    @EventHandler
    fun PlayerJoinEvent.handle() {
        player.gameMode = GameMode.SURVIVAL
    }

    @EventHandler
    fun BlockIgniteEvent.handle() {
        cancel()
    }

    @EventHandler
    fun EntityDamageEvent.handle() {
        if (entityType == EntityType.ARMOR_STAND || entityType == EntityType.ENDER_CRYSTAL) {
            cancel()
        }
    }

    @EventHandler
    fun CreatureSpawnEvent.handle() {
        if (spawnReason != CreatureSpawnEvent.SpawnReason.DEFAULT && entityType != EntityType.PLAYER && entityType != EntityType.ARMOR_STAND) {
            cancel()
        }
    }

    @EventHandler
    fun WorldLoadEvent.handle() {
        world.applySettings()
    }

    @EventHandler
    fun BlockBreakEvent.handle() {
        cancel()
    }

    @EventHandler
    fun BlockPlaceEvent.handle() {
        cancel()
    }

    private fun World.applySettings() {
        time = 6000
        isAutoSave = false
        setGameRuleValue("doDaylightCycle", "false")
        setGameRuleValue("doMobSpawning", "false")
        setGameRuleValue("announceAdvancements", "false")
        setGameRuleValue("spectatorsGenerateChunks", "false")
    }

    @EventHandler
    fun BlockFormEvent.handle() {
        val type = block.type
        if ((type == Material.WATER || type == Material.STATIONARY_WATER) && newState.type == Material.ICE) {
            cancel()
        }
    }
}
