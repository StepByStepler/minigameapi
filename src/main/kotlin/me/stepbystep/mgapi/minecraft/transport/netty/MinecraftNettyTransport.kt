package me.stepbystep.mgapi.minecraft.transport.netty

import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelOption
import io.netty.channel.epoll.Epoll
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollSocketChannel
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioSocketChannel
import me.stepbystep.mgapi.common.ServerID
import me.stepbystep.mgapi.common.packet.Packet
import me.stepbystep.mgapi.common.transport.netty.NettyChannelInitializer
import me.stepbystep.mgapi.common.util.writeAndFlushInEventLoop
import me.stepbystep.mgapi.minecraft.MinecraftActor
import me.stepbystep.mgapi.minecraft.transport.MinecraftMessageTransport
import org.bukkit.Bukkit
import java.util.concurrent.CompletableFuture

class MinecraftNettyTransport(private val actor: MinecraftActor) : MinecraftMessageTransport {

    private val channel = Bootstrap()
        .channel(if (Epoll.isAvailable()) EpollSocketChannel::class.java else NioSocketChannel::class.java)
        .group(if (Epoll.isAvailable()) EpollEventLoopGroup() else NioEventLoopGroup())
        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 1000)
        .handler(NettyChannelInitializer(actor))
        .remoteAddress(actor.coreAddress)
        .connect()
        .sync()
        .addListener {
            if (!it.isSuccess) {
                Bukkit.getLogger().severe("Could not connect to core at ${actor.coreAddress}! Shutting down server.")
                Bukkit.shutdown()
            }
        }
        .channel()

    private val clientID = ServerID(channel.id().asLongText())

    override fun <T : Packet<*, *>> sendPacket(packet: T): CompletableFuture<T> {
        val future = CompletableFuture<T>()
        actor.packetHandler.addFuture(packet, future)
        channel.writeAndFlushInEventLoop(packet).thenAccept { channelFuture ->
            channelFuture.addListener {
                if (!it.isSuccess) {
                    it.cause().printStackTrace()
                }
            }
        }
        return future
    }

    override fun getClientID(client: Any) = clientID

    override fun shutdown() {
        channel.disconnect().sync()
    }
}
