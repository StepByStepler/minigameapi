package me.stepbystep.mgapi.minecraft.transport

import me.stepbystep.mgapi.common.MessageTransport
import me.stepbystep.mgapi.common.packet.Packet
import java.util.concurrent.CompletableFuture

interface MinecraftMessageTransport : MessageTransport {
    fun <T : Packet<*, *>> sendPacket(packet: T): CompletableFuture<T>
    fun shutdown()
}
