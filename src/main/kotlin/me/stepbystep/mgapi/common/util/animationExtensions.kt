package me.stepbystep.mgapi.common.util

import com.google.common.cache.CacheBuilder
import me.func.mod.ui.menu.Storage
import me.func.mod.ui.scoreboard.ScoreBoard
import me.func.mod.ui.scoreboard.ScoreBoardScheme
import org.bukkit.entity.Player
import java.util.*
import java.util.concurrent.TimeUnit

fun ScoreBoardScheme.showSingle(player: Player) {
    ScoreBoard.unsubscribe(player)
    show(player)
}

private val lastOpens = CacheBuilder.newBuilder()
    .expireAfterWrite(500, TimeUnit.MILLISECONDS)
    .build<UUID, Pair<Long, String>>()

fun Storage.openDistinct(player: Player) {
    val lastData = lastOpens.getIfPresent(player.uniqueId)
    if (lastData?.second == title && System.currentTimeMillis() - lastData.first < 500) return

    lastOpens.put(player.uniqueId, System.currentTimeMillis() to title)
    open(player)
}