package me.stepbystep.mgapi.common.util

import me.func.mod.Anime
import me.stepbystep.api.chat.PMessage0
import me.stepbystep.mgapi.common.ServerID
import me.stepbystep.mgapi.common.network.NetworkMessenger
import me.stepbystep.mgapi.common.packet.Packet
import me.stepbystep.mgapi.common.packet.PacketHandler
import me.stepbystep.mgapi.common.packet.PacketListener
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import ru.cristalix.core.realm.RealmId
import ru.cristalix.core.transfer.ITransferService
import java.util.*
import java.util.concurrent.CompletableFuture
import kotlin.reflect.KClass

typealias PacketClass = KClass<out Packet<*, *>>

inline fun <reified T : Packet<*, *>> PacketHandler.handlePacket(crossinline action: (packet: T, serverID: ServerID) -> Unit) {
    addPacketListener(T::class, object : PacketListener<T> {
        override fun handle(packet: T, serverID: ServerID) {
            action(packet, serverID)
        }

        override fun handleWithResponse(packet: T, serverID: ServerID): CompletableFuture<T>? = null
    })
}

inline fun <reified T : Packet<*, *>> PacketHandler.handlePacketWithResponse(crossinline action: (packet: T, serverID: ServerID) -> CompletableFuture<T>) {
    addPacketListener(T::class, object : PacketListener<T> {
        override fun handle(packet: T, serverID: ServerID) {
            // nothing
        }

        override fun handleWithResponse(packet: T, serverID: ServerID): CompletableFuture<T> =
            action(packet, serverID)
    })
}

fun sendPlayerToHub(playerUUID: UUID) {
    ITransferService.get().transfer(playerUUID, RealmId.of("HUB", 0))
}

fun NetworkMessenger.sendMessage(playerUUID: UUID, message: PMessage0) {
    sendMessage(playerUUID, message(playerUUID))
}

inline fun broadcastTempMessage(supplyMessage: (Player) -> String) {
    for (player in Bukkit.getOnlinePlayers()) {
        Anime.killboardMessage(player, supplyMessage(player), 20)
    }
}
