package me.stepbystep.mgapi.common.util

import io.netty.buffer.ByteBuf
import io.netty.channel.Channel
import io.netty.channel.ChannelFuture
import java.util.concurrent.CompletableFuture

fun ByteBuf.writeString(str: String) {
    writeChar(str.length)
    for (char in str.toCharArray()) {
        writeChar(char.code)
    }
}

fun ByteBuf.readString(): String {
    val length = readChar().code
    val builder = StringBuilder(length)
    repeat(length) {
        builder.append(readChar())
    }
    return builder.toString()
}

fun Channel.writeAndFlushInEventLoop(data: Any): CompletableFuture<ChannelFuture> {
    val writeFuture = CompletableFuture<ChannelFuture>()

    if (eventLoop().inEventLoop()) {
        writeFuture.complete(writeAndFlush(data))
    } else {
        eventLoop().execute {
            writeFuture.complete(writeAndFlush(data))
        }
    }

    return writeFuture
}
