package me.stepbystep.mgapi.common

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.server.PluginDisableEvent

class ShutdownListener(private val actor: Actor<*>) : Listener {
    @EventHandler
    fun PluginDisableEvent.handle() {
        if (plugin == actor.plugin) {
            actor.shutdown()
        }
    }
}
