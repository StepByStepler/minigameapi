package me.stepbystep.mgapi.common.transport.netty

import io.netty.buffer.ByteBuf
import io.netty.channel.Channel
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.channel.ChannelInitializer
import io.netty.handler.codec.ByteToMessageDecoder
import io.netty.handler.codec.LengthFieldBasedFrameDecoder
import io.netty.handler.codec.LengthFieldPrepender
import io.netty.handler.codec.MessageToByteEncoder
import me.stepbystep.mgapi.common.Actor
import me.stepbystep.mgapi.common.packet.Packet
import me.stepbystep.mgapi.common.util.readString
import me.stepbystep.mgapi.common.util.writeString

open class NettyChannelInitializer(private val actor: Actor<*>) : ChannelInitializer<Channel>() {

    override fun initChannel(ch: Channel) {
        val lengthDecoder = LengthFieldBasedFrameDecoder(
            Int.MAX_VALUE,
            0,
            Int.SIZE_BYTES,
            0,
            Int.SIZE_BYTES
        )
        val lengthPrepender = LengthFieldPrepender(Int.SIZE_BYTES, false)
        ch.pipeline().addLast(lengthDecoder, lengthPrepender, PacketDecoder(), PacketEncoder(), PacketReceiver())
    }

    @OptIn(ExperimentalStdlibApi::class)
    inner class PacketDecoder : ByteToMessageDecoder() {
        override fun decode(ctx: ChannelHandlerContext, buf: ByteBuf, list: MutableList<Any>) {
            runCatching {
                val clazz = Class.forName(buf.readString())
                val dataArray = ByteArray(buf.readableBytes())
                buf.readBytes(dataArray)
                val packet = actor.packetSerializer.decode(dataArray, clazz)
                list.add(packet)
            }.onFailure { it.printStackTrace() }
        }
    }

    @Suppress("UNCHECKED_CAST")
    @OptIn(ExperimentalStdlibApi::class)
    inner class PacketEncoder : MessageToByteEncoder<Packet<*, *>>() {
        override fun encode(ctx: ChannelHandlerContext, packet: Packet<*, *>, buf: ByteBuf) {
            runCatching {
                buf.writeString(packet::class.java.name)
                val data = actor.packetSerializer.encode(packet)
                buf.writeBytes(data)
            }.onFailure { it.printStackTrace() }
        }
    }

    inner class PacketReceiver : ChannelInboundHandlerAdapter() {
        override fun channelRead(ctx: ChannelHandlerContext, msg: Any) {
            runCatching {
                check(msg is Packet<*, *>) { "Non-packet message received: $msg" }
                val messageTransport = actor.messageTransport
                val clientID = messageTransport.getClientID(ctx.channel())
                actor.packetHandler.onPacketReceive(clientID, msg, ctx::writeAndFlush)
            }.onFailure { it.printStackTrace() }
        }
    }
}
