package me.stepbystep.mgapi.common

import me.stepbystep.mgapi.client.GameState
import me.stepbystep.mgapi.common.game.GameType
import ru.cristalix.core.realm.RealmId

@JvmInline
value class ServerID(
    val id: String,
)

enum class ServerSide {
    Game,
    Lobby;
}

sealed class ServerInfo {
    abstract val realm: RealmId
    abstract val id: ServerID
    abstract val side: ServerSide

    var joinedPlayers = 0
    var canAcceptPlayers = true

    data class Game(
        override val realm: RealmId,
        override val id: ServerID,
        val gameType: GameType,
        val isHidden: Boolean,
    ) : ServerInfo() {
        var gameState = GameState.Default

        override val side: ServerSide get() = ServerSide.Game
    }

    data class Lobby(
        override val realm: RealmId,
        override val id: ServerID,
    ) : ServerInfo() {
        override val side: ServerSide get() = ServerSide.Lobby
    }
}
