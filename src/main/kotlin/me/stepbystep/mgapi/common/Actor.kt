package me.stepbystep.mgapi.common

import me.stepbystep.api.register
import me.stepbystep.mgapi.common.game.GameTypeHandler
import me.stepbystep.mgapi.common.network.CristalixNetworkMessenger
import me.stepbystep.mgapi.common.network.NetworkMessenger
import me.stepbystep.mgapi.common.packet.PacketHandler
import me.stepbystep.mgapi.common.packet.serializer.PacketSerializer
import me.stepbystep.mgapi.common.packet.serializer.gson.GsonPacketSerializer
import org.bukkit.plugin.Plugin
import org.spigotmc.SpigotConfig

abstract class Actor<T : MessageTransport>(
    val plugin: Plugin,
    val gameTypeHandler: GameTypeHandler,
) {
    val packetHandler = PacketHandler()
    var networkMessenger: NetworkMessenger = CristalixNetworkMessenger()
    var packetSerializer: PacketSerializer = GsonPacketSerializer()
    abstract var messageTransport: T

    init {
        SpigotConfig.disableAdvancementSaving = true
        SpigotConfig.disabledAdvancements = listOf("*")
    }

    internal abstract fun setup()

    internal abstract fun shutdown()

    abstract fun handleDefaultHandshake()

    open fun setupBukkit() {
        ShutdownListener(this).register(plugin)
    }
}
