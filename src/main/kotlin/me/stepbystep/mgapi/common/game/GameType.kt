package me.stepbystep.mgapi.common.game

import me.stepbystep.api.chat.PMessage0

interface GameType {
    val id: String
    val displayName: PMessage0
    val minPlayers: Int
    val maxPlayers: Int
}
