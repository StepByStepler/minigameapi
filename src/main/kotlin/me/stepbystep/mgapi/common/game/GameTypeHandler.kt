package me.stepbystep.mgapi.common.game

interface GameTypeHandler {
    val allTypes: List<GameType>
}
