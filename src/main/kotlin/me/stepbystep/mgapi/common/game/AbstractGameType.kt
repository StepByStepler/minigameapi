package me.stepbystep.mgapi.common.game

abstract class AbstractGameType(
    final override val id: String,
) : GameType {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AbstractGameType

        if (id != other.id) return false

        return true
    }

    override fun hashCode() = id.hashCode()
}
