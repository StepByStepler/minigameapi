package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.NoServerData
import me.stepbystep.mgapi.common.packet.Packet
import java.util.*

class SetPlayerNamePacket(clientData: ClientData) : Packet<SetPlayerNamePacket.ClientData, NoServerData>(clientData) {

    init {
        needsResponse = false
    }

    data class ClientData(val uuid: UUID, val name: String?) : IClientData
}
