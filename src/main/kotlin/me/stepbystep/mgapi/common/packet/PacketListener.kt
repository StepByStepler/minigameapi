package me.stepbystep.mgapi.common.packet

import me.stepbystep.mgapi.common.ServerID
import java.util.concurrent.CompletableFuture

interface PacketListener<T : Packet<*, *>> {
    fun handle(packet: T, serverID: ServerID)
    fun handleWithResponse(packet: T, serverID: ServerID): CompletableFuture<T>?
}
