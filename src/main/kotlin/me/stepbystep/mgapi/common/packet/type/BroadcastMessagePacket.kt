package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.api.chat.PMessage0
import me.stepbystep.api.chat.broadcast
import me.stepbystep.mgapi.common.packet.IServerData
import me.stepbystep.mgapi.common.packet.NoClientData
import me.stepbystep.mgapi.common.packet.Packet
import me.stepbystep.mgapi.common.util.handlePacket
import me.stepbystep.mgapi.minecraft.MinecraftActor

class BroadcastMessagePacket(serverData: ServerData) :
    Packet<NoClientData, BroadcastMessagePacket.ServerData>(serverData) {

    init {
        needsResponse = false
    }

    class ServerData(val message: PMessage0) : IServerData

    companion object {
        fun handleFor(actor: MinecraftActor) {
            actor.packetHandler.handlePacket<BroadcastMessagePacket> { packet, _ ->
                packet.serverData.message.broadcast()
            }
        }
    }
}
