package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.NoServerData
import me.stepbystep.mgapi.common.packet.Packet
import java.util.*

class SendPlayerToLobbyPacket(clientData: ClientData) : Packet<SendPlayerToLobbyPacket.ClientData, NoServerData>(clientData) {
    init {
        needsResponse = false
    }

    class ClientData(val playerUUID: UUID) : IClientData
}
