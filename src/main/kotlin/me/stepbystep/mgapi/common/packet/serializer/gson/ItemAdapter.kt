package me.stepbystep.mgapi.common.packet.serializer.gson

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import net.minecraft.server.v1_12_R1.Item

class ItemAdapter : TypeAdapter<Item>() {
    override fun write(writer: JsonWriter, item: Item) {
        writer.value(Item.getId(item))
    }

    override fun read(reader: JsonReader): Item = Item.getById(reader.nextInt())
}
