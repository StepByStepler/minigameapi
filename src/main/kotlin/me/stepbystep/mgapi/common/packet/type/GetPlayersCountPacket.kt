package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.IServerData
import me.stepbystep.mgapi.common.packet.Packet

class GetPlayersCountPacket : Packet<GetPlayersCountPacket.ClientData, GetPlayersCountPacket.ServerData> {
    constructor(clientData: ClientData) : super(clientData)
    constructor(serverData: ServerData) : super(serverData)

    class ClientData(val gameType: GameType) : IClientData
    data class ServerData(val totalPlayers: Int, val waitingPlayers: Int) : IServerData
}
