package me.stepbystep.mgapi.common.packet.serializer.gson

import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import me.stepbystep.api.NMSEntity

class EntityExclusionStrategy : ExclusionStrategy {
    override fun shouldSkipClass(clazz: Class<*>): Boolean =
        NMSEntity::class.java.isAssignableFrom(clazz)

    override fun shouldSkipField(fieldAttributes: FieldAttributes) = false
}
