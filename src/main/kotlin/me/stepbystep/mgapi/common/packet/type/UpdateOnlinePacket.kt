package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.NoServerData
import me.stepbystep.mgapi.common.packet.Packet

class UpdateOnlinePacket(clientData: ClientData) : Packet<UpdateOnlinePacket.ClientData, NoServerData>(clientData) {
    init {
        needsResponse = false
    }

    class ClientData(val onlinePlayers: Int) : IClientData
}
