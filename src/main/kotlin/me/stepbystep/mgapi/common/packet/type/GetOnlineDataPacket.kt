package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.model.OnlineDataEntry
import me.stepbystep.mgapi.common.packet.IServerData
import me.stepbystep.mgapi.common.packet.NoClientData
import me.stepbystep.mgapi.common.packet.Packet

class GetOnlineDataPacket : Packet<NoClientData, GetOnlineDataPacket.ServerData> {
    constructor() : super(NoClientData)
    constructor(serverData: ServerData) : super(serverData)

    data class ServerData(val servers: Map<GameType, OnlineDataEntry>) : IServerData
}