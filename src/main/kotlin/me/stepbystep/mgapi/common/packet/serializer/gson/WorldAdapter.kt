package me.stepbystep.mgapi.common.packet.serializer.gson

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import org.bukkit.Bukkit
import org.bukkit.World
import org.bukkit.WorldCreator

class WorldAdapter : TypeAdapter<World>() {
    override fun write(writer: JsonWriter, world: World) {
        writer.value(world.name)
    }

    override fun read(reader: JsonReader): World =
        Bukkit.createWorld(WorldCreator(reader.nextString()))
}
