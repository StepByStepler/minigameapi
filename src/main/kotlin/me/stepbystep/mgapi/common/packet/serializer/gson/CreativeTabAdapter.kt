package me.stepbystep.mgapi.common.packet.serializer.gson

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import net.minecraft.server.v1_12_R1.CreativeModeTab

class CreativeTabAdapter : TypeAdapter<CreativeModeTab>() {
    override fun write(writer: JsonWriter, tab: CreativeModeTab) {
        writer.value(tab.p)
    }

    override fun read(reader: JsonReader): CreativeModeTab {
        val key = reader.nextString()
        return CreativeModeTab.a.first { it.p == key }
    }
}
