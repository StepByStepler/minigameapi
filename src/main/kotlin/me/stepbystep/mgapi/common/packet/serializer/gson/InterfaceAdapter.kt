package me.stepbystep.mgapi.common.packet.serializer.gson

import com.google.gson.*
import java.lang.reflect.Type

class InterfaceAdapter : JsonSerializer<Any>, JsonDeserializer<Any> {
    private companion object {
        private const val CLASS_NAME_KEY = "CLASSNAME"
        private const val DATA_KEY = "DATA"
    }

    override fun serialize(jsonElement: Any, type: Type, ctx: JsonSerializationContext): JsonElement {
        val jsonObject = JsonObject()
        jsonObject.addProperty(CLASS_NAME_KEY, jsonElement::class.java.name)
        jsonObject.add(DATA_KEY, ctx.serialize(jsonElement))
        return jsonObject
    }

    override fun deserialize(jsonElement: JsonElement, type: Type, ctx: JsonDeserializationContext): Any {
        val jsonObject = jsonElement.asJsonObject
        val classPrimitive = jsonObject[CLASS_NAME_KEY] as JsonPrimitive
        val clazz = Class.forName(classPrimitive.asString)
        return ctx.deserialize(jsonObject.get(DATA_KEY), clazz)
    }
}
