package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.api.chat.PMessage0
import me.stepbystep.mgapi.common.ServerSide
import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.NoServerData
import me.stepbystep.mgapi.common.packet.Packet

class BroadcastMessageToServersPacket(clientData: ClientData) :
    Packet<BroadcastMessageToServersPacket.ClientData, NoServerData>(clientData) {

    class ClientData(
        val message: PMessage0,
        val skipSource: Boolean,
        vararg val sides: ServerSide,
    ) : IClientData
}
