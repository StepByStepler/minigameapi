package me.stepbystep.mgapi.common.packet

interface IClientData

interface IServerData

object NoClientData : IClientData

object NoServerData : IServerData
