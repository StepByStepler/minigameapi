package me.stepbystep.mgapi.common.packet.type

import com.google.common.cache.Cache
import com.mojang.authlib.GameProfile
import me.stepbystep.api.*
import me.stepbystep.api.chat.GREEN
import me.stepbystep.mgapi.common.packet.IServerData
import me.stepbystep.mgapi.common.packet.NoClientData
import me.stepbystep.mgapi.common.packet.Packet
import me.stepbystep.mgapi.common.util.handlePacket
import me.stepbystep.mgapi.lobby.command.UpdateNameCommand
import me.stepbystep.mgapi.minecraft.MinecraftActor
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerLoginEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.metadata.FixedMetadataValue
import ru.cristalix.core.CoreApi
import ru.cristalix.core.EventBus
import ru.cristalix.core.event.PermissionsLoadEvent
import ru.cristalix.core.permissions.*
import ru.cristalix.core.tab.ITabService
import java.lang.reflect.Field
import java.util.*
import kotlin.reflect.full.staticProperties

class SyncPlayerNamesPacket : Packet<NoClientData, SyncPlayerNamesPacket.ServerData> {
    constructor() : super(NoClientData)
    constructor(serverData: ServerData) : super(serverData)

    class ServerData(val playerNames: Map<UUID, String>) : IServerData

    companion object : Listener {
        fun register(actor: MinecraftActor) {
            JoinListener(actor).register(actor.plugin)
        }
    }

    class JoinListener(private val actor: MinecraftActor) : Listener {
        private companion object {
            private const val STEVE_SKIN_URL = "https://textures.minecraft.net/texture/963438953a782da769480b0a49125a92e259207700cb8e3e1aac38e471e20308"
            private const val STEVE_SKIN_HASH = "5366badc647ce7130e41d633b456b423"
        }

        private var customNames: Map<UUID, String> = emptyMap()
        private val donateGroups = DonateGroups::class.staticProperties.mapNotNull {
            it.get() as? IGroup
        }
        private val nameField = GameProfile::class.java.getDeclaredField("name").access()
        private val permissionsField = PermissionService::class.java.getDeclaredField("permissionMap").access()
        private val cacheField = PermissionService::class.java.getDeclaredField("cache").access()

        fun register() {
            val bus = CoreApi.get().bus() as EventBus<Class<*>>
            bus.register0<PermissionsLoadEvent>(
                this, PermissionsLoadEvent::class.java, { e ->
                    if (e.uuid in customNames) {
                        e.permissionContext.staffGroup = StaffGroups.PLAYER
                        e.permissionContext.donateGroup = donateGroups.random(RANDOM)
                        e.permissionContext.displayGroup = e.permissionContext.donateGroup
                    }
                }, 1
            )

            actor.messageTransport.sendPacket(SyncPlayerNamesPacket()).thenAccept {
                it.updateNames()
            }
            actor.packetHandler.handlePacket<SyncPlayerNamesPacket> { packet, _ ->
                packet.updateNames()
            }

            register(actor.plugin)
        }

        @EventHandler(priority = EventPriority.LOWEST)
        fun PlayerLoginEvent.handle() {
            val customName = customNames[player.uniqueId] ?: return
            nameField.set(player.asNMS().profile, customName)

            val metadata = FixedMetadataValue(actor.plugin, true)
            player.setMetadata(UpdateNameCommand.HIDDEN_METADATA, metadata)
            player.displayName = customName
            player.playerListName = customName
            player.asNMS().displayName = customName // TODO: check if we need it
            player.setSkinWithoutUpdate(STEVE_SKIN_URL, STEVE_SKIN_HASH)
        }

        @EventHandler(priority = EventPriority.MONITOR)
        fun PlayerQuitEvent.handle() {
            val uuid = player.uniqueId
            if (uuid in customNames) {
                unloadPermissions(uuid)
            }
        }

        private fun SyncPlayerNamesPacket.updateNames() {
            (customNames.keys - serverData.playerNames.keys).forEach { uuid -> disableHiddenNickname(uuid) }
            customNames = serverData.playerNames
            customNames.keys.forEach(::unloadPermissions)
        }

        private fun Field.access() = apply {
            isAccessible = true
        }

        private fun unloadPermissions(uuid: UUID) {
            val perms = IPermissionService.get() as PermissionService
            val permsMap = permissionsField[perms] as MutableMap<*, *>
            val permsCache = cacheField[perms] as Cache<*, *>
            permsMap.remove(uuid)
            permsCache.invalidate(uuid)
        }

        private fun disableHiddenNickname(uuid: UUID) {
            unloadPermissions(uuid)
            Bukkit.getPlayer(uuid)?.let { player ->
                ITabService.get().update(player)
                player.sendMessage("${GREEN}Перезайдите на сервер, чтобы вернуть свой ник и скин")
            }
        }
    }
}
