package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.NoServerData
import me.stepbystep.mgapi.common.packet.Packet
import java.util.*

class RemovePlayerFromQueuePacket(clientData: ClientData) : Packet<RemovePlayerFromQueuePacket.ClientData, NoServerData>(clientData) {

    init {
        needsResponse = false
    }

    class ClientData(val uuid: UUID) : IClientData
}
