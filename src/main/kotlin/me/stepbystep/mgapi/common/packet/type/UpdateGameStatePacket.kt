package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.client.GameState
import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.NoServerData
import me.stepbystep.mgapi.common.packet.Packet

class UpdateGameStatePacket(clientData: ClientData) : Packet<UpdateGameStatePacket.ClientData, NoServerData>(clientData) {
    init {
        needsResponse = false
    }

    data class ClientData(val gameState: GameState) : IClientData
}