package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.ServerID
import me.stepbystep.mgapi.common.ServerInfo
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.IServerData
import me.stepbystep.mgapi.common.packet.Packet
import ru.cristalix.core.realm.RealmId

class HandshakePacket : Packet<HandshakePacket.ClientData, HandshakePacket.ServerData> {

    constructor(clientData: ClientData) : super(clientData)
    constructor(serverData: ServerData) : super(serverData)

    open class ClientData(val data: Data) : IClientData

    open class ServerData : IServerData

    interface Data {
        val realmId: RealmId

        class Game(
            override val realmId: RealmId,
            val gameType: GameType,
            val isHidden: Boolean,
        ) : Data

        class Lobby(override val realmId: RealmId) : Data
    }

    fun createServerInfo(serverID: ServerID): ServerInfo {
        return when (val data = clientData.data) {
            is Data.Game -> ServerInfo.Game(data.realmId, serverID, data.gameType, data.isHidden)
            is Data.Lobby -> ServerInfo.Lobby(data.realmId, serverID)
            else -> error("Unknown handshake data: $data")
        }
    }
}
