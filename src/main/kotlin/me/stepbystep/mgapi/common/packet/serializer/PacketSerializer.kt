package me.stepbystep.mgapi.common.packet.serializer

import me.stepbystep.mgapi.common.packet.Packet

interface PacketSerializer {
    fun encode(packet: Packet<*, *>): ByteArray
    fun decode(data: ByteArray, clazz: Class<*>): Packet<*, *>
}
