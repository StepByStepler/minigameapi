package me.stepbystep.mgapi.common.packet.serializer.gson

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.WorldCreator

class LocationSerializer : TypeAdapter<Location>() {
    override fun write(writer: JsonWriter, loc: Location) {
        writer.beginObject()
        writer.value(loc.x)
        writer.value(loc.y)
        writer.value(loc.z)
        writer.value(loc.yaw)
        writer.value(loc.pitch)
        writer.endObject()
    }

    override fun read(reader: JsonReader): Location {
        reader.beginObject()

        return Location(
            Bukkit.createWorld(WorldCreator(reader.nextString())),
            reader.nextDouble(),
            reader.nextDouble(),
            reader.nextDouble(),
            reader.nextDouble().toFloat(),
            reader.nextDouble().toFloat()
        ).also {
            reader.endObject()
        }
    }
}
