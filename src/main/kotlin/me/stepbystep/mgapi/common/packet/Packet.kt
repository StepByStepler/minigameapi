package me.stepbystep.mgapi.common.packet

import java.util.*

abstract class Packet<C : IClientData, S : IServerData> {
    private var _clientData: IClientData? = null
    private var _serverData: IServerData? = null

    var uuid: UUID = UUID.randomUUID()

    var needsResponse = true

    constructor(clientData: C) {
        this._clientData = clientData
    }

    constructor(serverData: S) {
        this._serverData = serverData
    }

    @Suppress("UNCHECKED_CAST")
    val clientData: C
        get() = _clientData as C

    @Suppress("UNCHECKED_CAST")
    val serverData: S
        get() = _serverData as S
}
