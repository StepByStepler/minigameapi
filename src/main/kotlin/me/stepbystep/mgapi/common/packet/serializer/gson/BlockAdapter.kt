package me.stepbystep.mgapi.common.packet.serializer.gson

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import net.minecraft.server.v1_12_R1.Block

class BlockAdapter : TypeAdapter<Block>() {
    override fun write(writer: JsonWriter, block: Block) {
        writer.value(Block.getId(block))
    }

    override fun read(reader: JsonReader): Block = Block.getById(reader.nextInt())
}
