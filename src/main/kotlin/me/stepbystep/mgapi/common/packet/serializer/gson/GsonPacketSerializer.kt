package me.stepbystep.mgapi.common.packet.serializer.gson

import com.google.gson.GsonBuilder
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.IServerData
import me.stepbystep.mgapi.common.packet.Packet
import me.stepbystep.mgapi.common.packet.serializer.PacketSerializer
import me.stepbystep.mgapi.common.packet.type.HandshakePacket
import net.minecraft.server.v1_12_R1.Block
import net.minecraft.server.v1_12_R1.CreativeModeTab
import net.minecraft.server.v1_12_R1.Item
import net.minecraft.server.v1_12_R1.NBTBase
import org.bukkit.World
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld

@OptIn(ExperimentalStdlibApi::class)
class GsonPacketSerializer : PacketSerializer {

    private val builder = GsonBuilder()
        .registerTypeAdapter(IClientData::class.java, InterfaceAdapter())
        .registerTypeAdapter(IServerData::class.java, InterfaceAdapter())
        .registerTypeAdapter(GameType::class.java, InterfaceAdapter())
        .registerTypeAdapter(NBTBase::class.java, InterfaceAdapter())
        .registerTypeAdapter(HandshakePacket.Data::class.java, InterfaceAdapter())
        .registerTypeAdapter(World::class.java, WorldAdapter().nullSafe())
        .registerTypeAdapter(CraftWorld::class.java, WorldAdapter().nullSafe())
        .registerTypeAdapter(CreativeModeTab::class.java, CreativeTabAdapter().nullSafe())
        .registerTypeAdapter(Block::class.java, BlockAdapter().nullSafe())
        .registerTypeHierarchyAdapter(Item::class.java, ItemAdapter().nullSafe())
        .setExclusionStrategies(EntityExclusionStrategy())
        .setLenient()
        .enableComplexMapKeySerialization()

    private var gson = builder.create()

    override fun encode(packet: Packet<*, *>): ByteArray {
        return try {
            gson.toJson(packet).encodeToByteArray()
        } catch (t: Throwable) {
            println("Encoding $packet")
            throw t
        }
    }

    override fun decode(data: ByteArray, clazz: Class<*>): Packet<*, *> {
        return gson.fromJson<Packet<*, *>>(data.decodeToString(), clazz)
    }

    fun updateGson(action: (GsonBuilder) -> Unit) {
        action(builder)
        gson = builder.create()
    }
}
