package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.packet.NoClientData
import me.stepbystep.mgapi.common.packet.NoServerData
import me.stepbystep.mgapi.common.packet.Packet

class CoreRestartPacket : Packet<NoClientData, NoServerData>(NoServerData)
