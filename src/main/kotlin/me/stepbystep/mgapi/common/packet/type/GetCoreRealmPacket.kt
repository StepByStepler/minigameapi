package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.packet.IServerData
import me.stepbystep.mgapi.common.packet.NoClientData
import me.stepbystep.mgapi.common.packet.Packet
import ru.cristalix.core.realm.RealmId

class GetCoreRealmPacket : Packet<NoClientData, GetCoreRealmPacket.ServerData> {
    constructor() : super(NoClientData)
    constructor(serverData: ServerData) : super(serverData)

    class ServerData(val realmId: RealmId) : IServerData
}
