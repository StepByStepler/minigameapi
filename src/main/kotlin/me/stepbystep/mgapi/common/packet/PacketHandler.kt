package me.stepbystep.mgapi.common.packet

import com.google.common.cache.CacheBuilder
import com.google.common.cache.RemovalCause
import com.google.common.collect.HashMultimap
import me.stepbystep.mgapi.common.ServerID
import me.stepbystep.mgapi.common.packet.type.HandshakePacket
import me.stepbystep.mgapi.common.util.PacketClass
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong
import kotlin.system.measureTimeMillis

class PacketHandler {
    val packetsProfileData = ConcurrentHashMap<PacketClass, PacketProfileData>()
    var handshakeAction: ((ServerID, HandshakePacket) -> Unit)? = null
        internal set

    private val packetFutures = CacheBuilder.newBuilder()
        .expireAfterWrite(20, TimeUnit.SECONDS)
        .removalListener<UUID, CompletableFuture<Packet<*, *>>> {
            if (it.cause == RemovalCause.EXPIRED) {
                if (!it.value.isDone) {
                    println("Packet ${it.key} didn't receive response in 10 seconds!")
                }
            } else if (it.cause == RemovalCause.REPLACED) {
                it.value.completeExceptionally(IllegalStateException("Looks like UUID collation happened for ${it.key}"))
            }
        }.build<UUID, CompletableFuture<Packet<*, *>>>()

    private val packetListeners = HashMultimap.create<PacketClass, PacketListener<*>>()

    fun <T : Packet<*, *>> addFuture(packet: T, future: CompletableFuture<T>) {
        if (packet.needsResponse) {
            @Suppress("UNCHECKED_CAST")
            packetFutures.put(packet.uuid, future as CompletableFuture<Packet<*, *>>)
        }
    }

    fun <T : Packet<*, *>> onPacketReceive(serverID: ServerID, packet: T, writeResponse: (T) -> Unit) {
        packetFutures.getIfPresent(packet.uuid)?.let {
            it.complete(packet)
            packetFutures.invalidate(packet.uuid)
        }

        if (packet is HandshakePacket) {
            handshakeAction?.invoke(serverID, packet)
        }

        val profileData = packetsProfileData[packet::class] ?: run {
            val profileData = PacketProfileData()
            packetsProfileData[packet::class] = profileData
            profileData
        }
        val transactionTime = measureTimeMillis {
            packetListeners[packet::class].forEach {
                @Suppress("UNCHECKED_CAST")
                val listener = it as PacketListener<T>
                listener.handle(packet, serverID)
                listener.handleWithResponse(packet, serverID)?.thenAccept { response ->
                    response.needsResponse = false
                    response.uuid = packet.uuid
                    writeResponse(response)
                }
            }
        }
        profileData.processCount.incrementAndGet()
        profileData.totalTime.addAndGet(transactionTime)
        if (profileData.maxTime.get() < transactionTime) {
            profileData.maxTime.set(transactionTime)
        }
    }

    fun addPacketListener(clazz: PacketClass, listener: PacketListener<*>) {
        packetListeners.put(clazz, listener)
    }

    data class PacketProfileData(
        val totalTime: AtomicLong = AtomicLong(0),
        val processCount: AtomicLong = AtomicLong(0),
        val maxTime: AtomicLong = AtomicLong(0),
    )
}
