package me.stepbystep.mgapi.common.packet.type

import me.stepbystep.mgapi.common.packet.IClientData
import me.stepbystep.mgapi.common.packet.NoServerData
import me.stepbystep.mgapi.common.packet.Packet

class AcceptPlayersStatusPacket(clientData: ClientData) : Packet<AcceptPlayersStatusPacket.ClientData, NoServerData>(clientData) {
    init {
        needsResponse = false
    }

    class ClientData(val state: Boolean) : IClientData
}
