package me.stepbystep.mgapi.common

interface MessageTransport {
    fun getClientID(client: Any): ServerID
}
