package me.stepbystep.mgapi.common.model

data class OnlineDataEntry(
    val totalPlayers: Int,
    val waitingPlayers: Int,
    val totalServers: Int,
)