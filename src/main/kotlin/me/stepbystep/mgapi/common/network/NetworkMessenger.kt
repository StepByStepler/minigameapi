package me.stepbystep.mgapi.common.network

import ru.cristalix.core.realm.RealmId
import java.util.*

interface NetworkMessenger {
    fun sendPlayerToRealm(playerUUID: UUID, realmId: RealmId)
    fun sendMessage(playerUUID: UUID, text: String)
}
