package me.stepbystep.mgapi.common.network

import me.stepbystep.api.registerServiceIfAbsent
import net.md_5.bungee.api.ChatMessageType
import org.bukkit.Bukkit
import ru.cristalix.core.CoreApi
import ru.cristalix.core.message.BukkitMessageService
import ru.cristalix.core.message.IMessageService
import ru.cristalix.core.message.Message
import ru.cristalix.core.realm.IRealmService
import ru.cristalix.core.realm.RealmId
import ru.cristalix.core.realm.RealmStatus
import ru.cristalix.core.transfer.ITransferService
import ru.cristalix.core.transfer.TransferService
import java.util.*

class CristalixNetworkMessenger : NetworkMessenger {

    init {
        registerServiceIfAbsent {
            TransferService(CoreApi.get().socketClient)
        }
        registerServiceIfAbsent {
            BukkitMessageService(CoreApi.get().socketClient, Bukkit.getServer(), IRealmService.get())
        }

        IRealmService.get().currentRealmInfo.status = RealmStatus.WAITING_FOR_PLAYERS
    }

    override fun sendPlayerToRealm(playerUUID: UUID, realmId: RealmId) {
        ITransferService.get().transfer(playerUUID, realmId)
    }

    override fun sendMessage(playerUUID: UUID, text: String) {
        val message = Message(arrayListOf(playerUUID), ChatMessageType.CHAT, text, null)
        IMessageService.get().sendMessage(message)
    }
}
