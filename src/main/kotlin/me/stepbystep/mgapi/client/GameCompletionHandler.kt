package me.stepbystep.mgapi.client

import org.bukkit.Bukkit

enum class GameCompletionHandler {
    ShutdownServer {
        override fun run() {
            Bukkit.shutdown()
        }
    },
    ReloadWorlds {
        override fun run() {
            Bukkit.getWorlds()
                .flatMap { it.loadedChunks.asIterable() }
                .forEach { it.unload(false) }
        }
    },
    Nothing {
        override fun run() {
            // nothing
        }
    };

    abstract fun run()
}
