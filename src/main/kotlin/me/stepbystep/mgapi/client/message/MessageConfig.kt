package me.stepbystep.mgapi.client.message

import me.stepbystep.api.chat.PMessage0
import me.stepbystep.api.chat.PMessage1
import me.stepbystep.api.chat.PMessage3
import org.bukkit.entity.Player

interface MessageConfig {
    val playerJoin: PMessage3<Int, Int, String>
    val playerQuitLobby: PMessage3<Int, Int, String>
    val playerQuitGame: PMessage1<Player>
    val countdown: PMessage1<Int>
    val countdownInterrupted: PMessage0
    val gameStarted: PMessage0
}
