package me.stepbystep.mgapi.client.message

import me.stepbystep.api.chat.*
import me.stepbystep.api.englishWordForNum
import me.stepbystep.api.wordForNum
import org.bukkit.entity.Player

open class DefaultMessageConfig : MessageConfig {
    override val playerJoin = message3<Int, Int, String>(
        russian = { players, maxPlayers, name ->
            "$GRAY[$GREEN$players$GRAY/$GOLD$maxPlayers$GRAY] ${GREEN}Игрок $GOLD$name ${GREEN}зашел в игру"
        },
        english = { players, maxPlayers, name ->
            "$GRAY[$GREEN$players$GRAY/$GOLD$maxPlayers$GRAY] ${GREEN}Player $GOLD$name ${GREEN}joined the game"
        },
    )

    override val playerQuitLobby = message3<Int, Int, String>(
        russian = { players, maxPlayers, name ->
            "$GRAY[$GREEN$players$GRAY/$GOLD$maxPlayers$GRAY] ${RED}Игрок $GOLD$name ${RED}покинул игру"
        },
        english = { players, maxPlayers, name ->
            "$GRAY[$GREEN$players$GRAY/$GOLD$maxPlayers$GRAY] ${RED}Player $GOLD$name ${RED}left the game"
        },
    )

    override val playerQuitGame = message1<Player>(
        russian = { "${RED}Игрок $GOLD${it.name} ${RED}покинул игру" },
        english = { "${RED}Player $GOLD${it.name} ${RED}left the game" },
    )

    override val countdown = message1<Int>(
        russian = { secondsLeft ->
            val secWord = secondsLeft.wordForNum("секунду", "секунды", "секунд")
            "${GREEN}Игра начнется через $GOLD$secondsLeft ${GREEN}$secWord"
        },
        english = { secondsLeft ->
            val secWord = secondsLeft.englishWordForNum("second", "seconds")
            "${GREEN}Game will start in $GOLD$secondsLeft ${GREEN}$secWord"
        },
    )

    override val countdownInterrupted = message0(
        russian = "${RED}Отсчет до начала игры прерван",
        english = "${RED}Countdown was interrupted",
    )

    override val gameStarted = message0(
        russian = "${GREEN}Игра началась",
        english = "${GREEN}Game started",
    )
}
