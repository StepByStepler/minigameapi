package me.stepbystep.mgapi.client.countdown

import me.stepbystep.api.chat.broadcast
import me.stepbystep.api.seconds
import me.stepbystep.mgapi.client.ClientActor

class ChatMessageCountdown(private val actor: ClientActor) : Countdown {
    override val period = 1.seconds

    override fun tick(tickOrdinal: Int) {
        val secondsLeft = actor.countdownSeconds - tickOrdinal
        if (secondsLeft % 10 == 0 || secondsLeft < 10) {
            actor.messageConfig.countdown.broadcast(secondsLeft)
        }
    }
}
