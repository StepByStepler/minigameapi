package me.stepbystep.mgapi.client.countdown

import kotlin.time.Duration

interface Countdown {
    val period: Duration
    fun tick(tickOrdinal: Int)

    fun start() {
        // nothing
    }

    fun stop() {
        // nothing
    }
}

operator fun Countdown.plus(other: Countdown): Countdown {
    require(period == other.period) { "Combined countdowns cannot have different periods!" }
    return object : Countdown {
        override val period = other.period

        override fun tick(tickOrdinal: Int) {
            this@plus.tick(tickOrdinal)
            other.tick(tickOrdinal)
        }

        override fun start() {
            this@plus.start()
            other.start()
        }

        override fun stop() {
            this@plus.stop()
            other.stop()
        }
    }
}
