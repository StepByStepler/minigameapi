package me.stepbystep.mgapi.client.countdown

import me.stepbystep.api.inTicksLong
import me.stepbystep.api.seconds
import me.stepbystep.mgapi.client.ClientActor
import org.bukkit.scheduler.BukkitRunnable

class CountdownRunnable(private val actor: ClientActor) : BukkitRunnable() {

    internal var ticksPassed = 0

    var secondsLeft: Double
        get() {
            val secondsInTick = actor.countdown.period / 1.seconds
            return actor.countdownSeconds - ticksPassed * secondsInTick
        }
        set(value) {
            val secondsPassed = actor.countdownSeconds - value
            val ticksInSecond = 1.seconds / actor.countdown.period
            ticksPassed = (secondsPassed * ticksInSecond).toInt()
        }

    fun start() = apply {
        actor.countdown.start()
        runTaskTimer(actor.plugin, 0, actor.countdown.period.inTicksLong)
    }

    fun shouldKick(): Boolean = (actor.countdownSeconds - ticksPassed) < actor.maxCountdownOnJoin

    override fun run() {
        actor.countdown.tick(ticksPassed++)

        if (shouldKick()) {
            actor.acceptPlayers(false)
        }

        if (secondsLeft <= 0.0) {
            actor.startGame()
            cancel()
        }
    }

    override fun cancel() {
        super.cancel()
        actor.countdown.stop()
    }
}
