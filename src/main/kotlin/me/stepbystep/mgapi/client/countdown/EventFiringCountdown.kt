package me.stepbystep.mgapi.client.countdown

import me.stepbystep.api.seconds
import me.stepbystep.mgapi.client.event.CountdownTickEvent

class EventFiringCountdown : Countdown {
    override val period get() = 1.seconds

    override fun tick(tickOrdinal: Int) {
        CountdownTickEvent(tickOrdinal).callEvent()
    }
}
