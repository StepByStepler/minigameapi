package me.stepbystep.mgapi.client.countdown

import me.stepbystep.api.chat.YELLOW
import me.stepbystep.api.chat.message0
import me.stepbystep.api.seconds
import me.stepbystep.api.sendProgressCompat
import me.stepbystep.mgapi.client.ClientActor
import org.bukkit.Bukkit
import ru.cristalix.core.display.IDisplayService
import ru.cristalix.core.display.enums.EnumPosition
import ru.cristalix.core.display.enums.EnumUpdateType
import ru.cristalix.core.display.messages.ProgressMessage
import ru.cristalix.core.formatting.Color

class BossBarCountdown(private val actor: ClientActor) : Countdown {
    override val period = 1.seconds

    private val bossBarTitle = message0(
        russian = "${YELLOW}До начала игры",
        english = "${YELLOW}Until game beginning",
    )

    override fun tick(tickOrdinal: Int) {
        val percent = tickOrdinal.toFloat() / actor.countdownSeconds
        val reversedPercent = 1 - percent
        broadcastProgressMessage(reversedPercent, EnumUpdateType.ADD)
    }

    override fun start() {
        broadcastProgressMessage(0f, EnumUpdateType.ADD)
    }

    override fun stop() {
        broadcastProgressMessage(0f, EnumUpdateType.REMOVE)
    }

    private fun broadcastProgressMessage(percent: Float, updateType: EnumUpdateType) {
        ProgressMessage.builder()
            .updateType(updateType)
            .color(Color.GREEN)
            .position(EnumPosition.TOPTOP)
            .percent(percent)
            .build()
            .apply {
                IDisplayService.get().sendProgressCompat(Bukkit.getOnlinePlayers(), bossBarTitle, this)
            }
    }
}
