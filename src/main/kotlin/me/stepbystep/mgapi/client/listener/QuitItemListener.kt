package me.stepbystep.mgapi.client.listener

import me.stepbystep.api.asNMS
import me.stepbystep.api.cancel
import me.stepbystep.api.chat.GREEN
import me.stepbystep.api.chat.message0
import me.stepbystep.api.isLeftClick
import me.stepbystep.api.item.ItemTag
import me.stepbystep.api.item.createNMSItem
import me.stepbystep.api.item.getNullableBoolean
import me.stepbystep.api.unregister
import me.stepbystep.mgapi.client.ClientActor
import me.stepbystep.mgapi.client.event.GameStartEvent
import net.minecraft.server.v1_12_R1.NBTTagByte
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryDragEvent
import org.bukkit.event.player.PlayerDropItemEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerJoinEvent

class QuitItemListener(
    private val itemType: Material,
    private val actor: ClientActor,
) : Listener {

    private companion object {
        private const val ITEM_TAG = "quitItem"
    }

    private object Messages {
        val quitToLobby = message0(
            russian = "${GREEN}Выйти в лобби",
            english = "${GREEN}Quit to lobby",
        )
    }

    @EventHandler(priority = EventPriority.HIGH)
    fun PlayerJoinEvent.handle() {
        val item = createNMSItem(
            material = itemType,
            displayName = Messages.quitToLobby(player),
            customTag = ItemTag(ITEM_TAG, NBTTagByte(1))
        )
        player.asNMS().inventory.setItem(8, item)
    }

    @EventHandler
    fun InventoryClickEvent.handle() {
        cancel()
    }

    @EventHandler
    fun InventoryDragEvent.handle() {
        cancel()
    }

    @EventHandler
    @Suppress("UNUSED")
    fun GameStartEvent.handle() {
        unregister()
        actor.getOnlinePlayers().forEach {
            it.inventory.setItem(8, null)
        }
    }

    @EventHandler
    fun PlayerInteractEvent.handle() {
        if (action.isLeftClick()) return
        val tag = item?.asNMS()?.tag ?: return

        if (tag.getNullableBoolean(ITEM_TAG) == true) {
            actor.sendPlayerToLobby(player)
        }
    }

    @EventHandler
    fun PlayerDropItemEvent.handle() {
        cancel()
    }
}
