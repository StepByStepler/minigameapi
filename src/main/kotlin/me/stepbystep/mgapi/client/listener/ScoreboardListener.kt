package me.stepbystep.mgapi.client.listener

import me.func.mod.ui.scoreboard.ScoreBoard
import me.stepbystep.api.chat.GREEN
import me.stepbystep.api.chat.RED
import me.stepbystep.api.chat.WHITE
import me.stepbystep.api.runDelayed
import me.stepbystep.api.ticks
import me.stepbystep.mgapi.client.ClientActor
import me.stepbystep.mgapi.common.util.showSingle
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class ScoreboardListener(
    minigameName: String,
    private val actor: ClientActor,
) : Listener {
    private val scoreboard = ScoreBoard.builder()
        .header(minigameName)
        .key("minigameBoard")
        .dynamic("Игроков") {
            "$GREEN${Bukkit.getOnlinePlayers().size}$WHITE/$RED${actor.gameType.maxPlayers}"
        }
        .line("Для старта нужно", "$GREEN${actor.gameType.minPlayers}")
        .build()

    @EventHandler
    fun PlayerJoinEvent.handle() {
        actor.plugin.runDelayed(2.ticks) {
            scoreboard.showSingle(player)
        }
    }
}
