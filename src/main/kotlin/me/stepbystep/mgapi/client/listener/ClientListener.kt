package me.stepbystep.mgapi.client.listener

import me.stepbystep.api.cancel
import me.stepbystep.api.chat.*
import me.stepbystep.mgapi.client.ClientActor
import me.stepbystep.mgapi.client.GameState
import me.stepbystep.mgapi.client.countdown.CountdownRunnable
import me.stepbystep.mgapi.common.packet.type.BanPlayerPacket
import me.stepbystep.mgapi.common.packet.type.UpdateOnlinePacket
import me.stepbystep.mgapi.common.util.broadcastTempMessage
import org.bukkit.GameMode
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.event.player.*
import java.util.*

class ClientListener(private val actor: ClientActor) : Listener {

    private val loggedPlayers = hashSetOf<UUID>()

    private object Messages {
        val serverFull = message0(
            russian = "Сервер заполнен",
            english = "Server is full",
        )

        val colorizedServerFull = serverFull.map { "${RED}$it" }
    }

//    @EventHandler
    fun AsyncPlayerPreLoginEvent.handle() {
        if (shouldDisallowJoin()) {
            val kickMessage = when (val uuid = playerProfile.id) {
                null -> Messages.serverFull.russian
                else -> Messages.serverFull(uuid)
            }

            disallow(AsyncPlayerPreLoginEvent.Result.KICK_FULL, kickMessage)
            actor.acceptPlayers(false)
        }
    }

//    @EventHandler
    fun PlayerLoginEvent.handle() {
        if (shouldDisallowJoin()) {
            disallow(PlayerLoginEvent.Result.KICK_FULL, Messages.serverFull(player.uniqueId))
            actor.acceptPlayers(false)
        }
    }

    @EventHandler
    fun PlayerJoinEvent.handle() {
//        if (shouldDisallowJoin()) {
//            actor.sendPlayerToLobby(player)
//            player.sendMessage(Messages.colorizedServerFull)
//            return
//        }

        loggedPlayers.add(player.uniqueId)
        updateOnline()
        if (actor.isGameStarted) return

        joinMessage = null
        broadcastTempMessage {
            actor.messageConfig.playerJoin(it, loggedPlayers.size, actor.gameType.maxPlayers, player.name)
        }
        player.inventory.clear()

        if (loggedPlayers.size >= actor.gameType.minPlayers && actor.countdownRunnable == null && actor.isAutoStart) {
            actor.countdownRunnable = CountdownRunnable(actor).start()
        }

        if (loggedPlayers.size >= actor.gameType.maxPlayers) {
            actor.acceptPlayers(false)

            val countdownRunnable = actor.countdownRunnable ?: error("Countdown didn't start when server is full")
            countdownRunnable.apply {
                secondsLeft = secondsLeft.coerceAtMost(actor.secondsWhenFull.toDouble())
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    fun PlayerQuitEvent.handle() {
        if (player.uniqueId !in loggedPlayers) return

        loggedPlayers -= player.uniqueId
        updateOnline()
        broadcastQuitMessage()
        player.banIfNeed()

        if (actor.isGameStarted) return

        val countdownRunnable = actor.countdownRunnable
        if (countdownRunnable?.shouldCancelCountdown() == true) {
            countdownRunnable.cancel()
            actor.countdownRunnable = null
            actor.messageConfig.countdownInterrupted.broadcast()

            actor.acceptPlayers(true)
        }

        if (!shouldDisallowJoin()) {
            actor.acceptPlayers(true)
        }
    }

    @EventHandler
    fun PlayerDropItemEvent.handle() {
        if (actor.gameState == GameState.Waiting) {
            cancel()
        }
    }

    @EventHandler
    fun EntityDamageEvent.handle() {
        if (!actor.gameState.isActive) {
            cancel()
        }
    }

    private fun CountdownRunnable.shouldCancelCountdown(): Boolean =
        !actor.isGameStarted && loggedPlayers.size < actor.gameType.minPlayers && !shouldKick()

    private fun PlayerQuitEvent.broadcastQuitMessage() {
        quitMessage = null
        if (player.gameMode == GameMode.SPECTATOR) return
        broadcastTempMessage {
            if (actor.isGameStarted)
                actor.messageConfig.playerQuitGame(it, player)
            else
                actor.messageConfig.playerQuitLobby(it, loggedPlayers.size, actor.gameType.maxPlayers, player.name)
        }
    }

    private fun Player.banIfNeed() {
        if (!actor.banPlayerOnQuit) return
        if (!actor.gameState.isActive) return
        if (!actor.isPlayerAlive(this)) return

        val packet = BanPlayerPacket(BanPlayerPacket.ClientData(uniqueId))
        actor.messageTransport.sendPacket(packet)
    }

    private fun updateOnline() {
        val clientData = UpdateOnlinePacket.ClientData(loggedPlayers.size)
        actor.messageTransport.sendPacket(UpdateOnlinePacket(clientData))
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun shouldDisallowJoin(): Boolean =
        loggedPlayers.size >= actor.gameType.maxPlayers ||
            (actor.countdownRunnable?.shouldKick() == true) ||
            actor.disallowJoinCondition()
}
