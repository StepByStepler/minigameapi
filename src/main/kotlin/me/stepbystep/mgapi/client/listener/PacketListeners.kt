package me.stepbystep.mgapi.client.listener

import me.stepbystep.mgapi.client.ClientActor
import me.stepbystep.mgapi.client.event.GameForcedStopEvent
import me.stepbystep.mgapi.common.packet.type.BroadcastMessagePacket
import me.stepbystep.mgapi.common.packet.type.CoreRestartPacket
import me.stepbystep.mgapi.common.packet.type.ScheduleRestartPacket
import me.stepbystep.mgapi.common.util.handlePacket
import org.bukkit.Bukkit

internal fun ClientActor.registerPacketListeners() {
    BroadcastMessagePacket.handleFor(this)

    packetHandler.handlePacket<CoreRestartPacket> { _, _ ->
        GameForcedStopEvent(this).callEvent()
    }

    packetHandler.handlePacket<ScheduleRestartPacket> { _, _ ->
        if (Bukkit.getOnlinePlayers().isEmpty()) {
            Bukkit.shutdown()
        } else {
            needsRestart = true
        }
    }
}
