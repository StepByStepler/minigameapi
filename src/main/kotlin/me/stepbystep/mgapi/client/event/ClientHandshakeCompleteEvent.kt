package me.stepbystep.mgapi.client.event

import me.stepbystep.mgapi.client.ClientActor
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

class ClientHandshakeCompleteEvent(val actor: ClientActor) : Event() {
    companion object {
        private val handlerList = HandlerList()

        @JvmStatic
        fun getHandlerList() = handlerList
    }

    override fun getHandlers() = handlerList
}