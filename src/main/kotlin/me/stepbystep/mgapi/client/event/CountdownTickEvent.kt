package me.stepbystep.mgapi.client.event

import org.bukkit.event.Event
import org.bukkit.event.HandlerList

class CountdownTickEvent(val tickOrdinal: Int) : Event() {
    companion object {
        private val handlerList = HandlerList()

        @JvmStatic
        fun getHandlerList() = handlerList
    }

    override fun getHandlers() = handlerList
}
