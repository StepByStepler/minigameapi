package me.stepbystep.mgapi.client.command

import me.stepbystep.api.chat.GREEN
import me.stepbystep.api.chat.message0
import me.stepbystep.api.chat.sendMessage
import me.stepbystep.api.command.executesWrapped
import me.stepbystep.api.command.register
import me.stepbystep.api.command.sender
import me.stepbystep.mgapi.client.ClientActor
import ru.cristalix.core.command.CommandHelper

class LobbyCommand(actor: ClientActor) {

    init {
        val lobbyMessage = message0(
            russian = "${GREEN}Вы переместились в лобби",
            english = "${GREEN}You moved to lobby",
        )

        CommandHelper.literal("lobby")
            .executesWrapped {
                actor.sendPlayerToLobby(it.sender)
                it.sender.sendMessage(lobbyMessage)
            }
            .register()
    }
}
