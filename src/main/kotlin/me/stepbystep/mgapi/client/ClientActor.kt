package me.stepbystep.mgapi.client

import me.stepbystep.api.chat.broadcast
import me.stepbystep.api.chat.message0
import me.stepbystep.api.register
import me.stepbystep.api.runDelayed
import me.stepbystep.api.seconds
import me.stepbystep.mgapi.client.command.LobbyCommand
import me.stepbystep.mgapi.client.countdown.ChatMessageCountdown
import me.stepbystep.mgapi.client.countdown.Countdown
import me.stepbystep.mgapi.client.countdown.CountdownRunnable
import me.stepbystep.mgapi.client.event.ClientHandshakeCompleteEvent
import me.stepbystep.mgapi.client.event.GameStartEvent
import me.stepbystep.mgapi.client.listener.ClientListener
import me.stepbystep.mgapi.client.listener.QuitItemListener
import me.stepbystep.mgapi.client.listener.ScoreboardListener
import me.stepbystep.mgapi.client.listener.registerPacketListeners
import me.stepbystep.mgapi.client.message.DefaultMessageConfig
import me.stepbystep.mgapi.client.message.MessageConfig
import me.stepbystep.mgapi.common.game.GameType
import me.stepbystep.mgapi.common.game.GameTypeHandler
import me.stepbystep.mgapi.common.packet.type.AcceptPlayersStatusPacket
import me.stepbystep.mgapi.common.packet.type.HandshakePacket
import me.stepbystep.mgapi.common.packet.type.SendPlayerToLobbyPacket
import me.stepbystep.mgapi.common.packet.type.UpdateGameStatePacket
import me.stepbystep.mgapi.minecraft.MinecraftActor
import net.minecraft.server.v1_12_R1.MinecraftServer
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin
import ru.cristalix.core.realm.IRealmService
import kotlin.properties.Delegates

@Suppress("unused", "MemberVisibilityCanBePrivate")
class ClientActor @PublishedApi internal constructor(
    plugin: Plugin,
    gameTypeHandler: GameTypeHandler,
    val gameType: GameType,
) : MinecraftActor(plugin, gameTypeHandler) {

    var messageConfig: MessageConfig = DefaultMessageConfig()
    var countdown: Countdown = ChatMessageCountdown(this)
    var gameCompletionHandler: GameCompletionHandler = GameCompletionHandler.Nothing
    var countdownSeconds = 10
    var secondsWhenFull = Int.MAX_VALUE
    var maxCountdownOnJoin = Int.MAX_VALUE
    var isPlayerAlive: (Player) -> Boolean = { true }
    var disallowJoinCondition: () -> Boolean = { false }
    var isHidden: Boolean = false
    var isAutoStart: Boolean = true
    var banPlayerOnQuit: Boolean = true
    var activeGameState: GameState = GameState.Active
    var getOnlinePlayers: () -> Collection<Player> = { Bukkit.getOnlinePlayers() }

    lateinit var clientListener: ClientListener

    var gameState: GameState by Delegates.observable(GameState.Default) { _, _, newValue ->
        IRealmService.get().currentRealmInfo.status = newValue.cristalixStatus
        IRealmService.get().update()
        messageTransport.sendPacket(UpdateGameStatePacket(UpdateGameStatePacket.ClientData(newValue)))
    }
    val isGameStarted: Boolean get() = gameState.isStarted

    var countdownRunnable: CountdownRunnable? = null

    internal var needsRestart = false

    private var scoreboardListener: ScoreboardListener? = null
    private var quitItemListener: QuitItemListener? = null

    private var acceptPlayers = true

    @PublishedApi
    override fun setup() {
        check(gameType in gameTypeHandler.allTypes) {
            "$gameType not included in ${gameTypeHandler.allTypes}"
        }
        registerPacketListeners()
        MinecraftServer.SERVER.playerList.maxPlayers = gameType.maxPlayers
        LobbyCommand(this)
    }

    override fun setupBukkit() {
        super.setupBukkit()

        clientListener = ClientListener(this).also { it.register(plugin) }
        scoreboardListener?.register(plugin)
        quitItemListener?.register(plugin)
    }

    override fun shutdown() {
        acceptPlayers(false)
        messageTransport.shutdown()
    }

    override fun handleDefaultHandshake() {
        val data = HandshakePacket.Data.Game(realmId, gameType, isHidden)
        val packet = HandshakePacket(HandshakePacket.ClientData(data))
        messageTransport.sendPacket(packet).thenAccept {
            ClientHandshakeCompleteEvent(this).callEvent()
        }
    }

    internal fun startGame() {
        gameState = activeGameState
        countdownRunnable = null
        messageConfig.gameStarted.broadcast()
        GameStartEvent(this).callEvent()
        acceptPlayers(false)
    }

    fun completeGame() {
        Bukkit.getOnlinePlayers().forEach(::sendPlayerToLobby)

        plugin.runDelayed(3.seconds) {
            Bukkit.getOnlinePlayers().forEach {
                it.kickPlayer(Messages.gameFinished(it))
            }
            gameState = GameState.Waiting
            if (needsRestart) {
                Bukkit.shutdown()
            } else {
                gameCompletionHandler.run()
                acceptPlayers(true)
            }
        }
    }

    fun sendPlayerToLobby(player: Player) {
        val packet = SendPlayerToLobbyPacket(SendPlayerToLobbyPacket.ClientData(player.uniqueId))
        messageTransport.sendPacket(packet)
    }

    fun enableScoreboard(minigameName: String) {
        scoreboardListener = ScoreboardListener(minigameName, this)
    }

    fun enableQuitItem(type: Material) {
        quitItemListener = QuitItemListener(type, this)
    }

    fun disableAutoStart() {
        isAutoStart = false
    }

    fun onGameFinishing() {
        gameState = GameState.Finishing
    }

    fun acceptPlayers(state: Boolean) {
        if (acceptPlayers != state) {
            acceptPlayers = state
            messageTransport.sendPacket(AcceptPlayersStatusPacket(AcceptPlayersStatusPacket.ClientData(state)))
        }
    }

    private object Messages {
        val gameFinished = message0(
            russian = "Игра закончилась",
            english = "Game finished",
        )
    }

    companion object {
        inline operator fun invoke(
            plugin: Plugin,
            gameTypeHandler: GameTypeHandler,
            gameType: GameType = gameTypeHandler.allTypes.random(),
            action: ClientActor.() -> Unit = {},
        ): ClientActor {
            val actor = ClientActor(plugin, gameTypeHandler, gameType)
            action(actor)
            actor.setup()
            return actor
        }
    }
}
