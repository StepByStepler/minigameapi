package me.stepbystep.mgapi.client

import ru.cristalix.core.realm.RealmStatus

enum class GameState(val cristalixStatus: RealmStatus) {
    Waiting(RealmStatus.WAITING_FOR_PLAYERS),
    Active(RealmStatus.GAME_STARTED_CAN_SPACTATE),
    ActiveNoSpectate(RealmStatus.GAME_STARTED_RESTRICTED),
    Finishing(RealmStatus.GAME_ENDING);

    val isStarted: Boolean get() = isActive || this == Finishing
    val isActive: Boolean get() = this == Active || this == ActiveNoSpectate

    companion object {
        val Default: GameState get() = Waiting
    }
}
